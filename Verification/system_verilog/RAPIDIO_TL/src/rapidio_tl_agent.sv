/*---------------------------------------------------------------------------------------------------------------------------------------------------
-- Copyright (c) 2013, 2014 Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:--
1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following  disclaimer.-- 
2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and -- the following disclaimer in the documentation and/or other materials provided with the distribution.-- 
3. Neither the name of IIT Madras nor the names of its contributors may be used to endorse or  -- promote products derived from this software without specific prior written permission.

--- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, -- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. -- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, -- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; -- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, -- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.-- -----------------------------------------------------------------------------------------------------------------------------------------------------*/
`ifndef RAPIDIO_TL_AGENT_SVH
`define RAPIDIO_TL_AGENT_SVH

class rapidio_tl_agent extends uvm_component;
 
 
  // Data members
  rapidio_tl_agent_config m_config;
  rapidio_adapter rap_tl_adp; 
 
  // Component members
  // Create the objects of driver, monitor and sequencer
  uvm_blocking_put_port #(rapidio_tl_sequence_item) logic2trans_req_port;//assumption 
  uvm_blocking_put_port #(rapidio_tl_sequence_item) trans2logic_req_port; 
  uvm_blocking_put_port #(rapidio_tl_sequence_item) trans2phy_req_port; 
  uvm_analysis_port #(rapidio_tl_sequence_item) tl_monitor_port; 

 // uvm_analysis_port #(rapidio_tl_sequence_item) trans2phy_resp_port;
  uvm_analysis_port #(rapidio_tl_sequence_item) tl_cov_port; 
  uvm_analysis_port #(rapidio_tl_sequence_item) tl_scbd_port;
  uvm_tlm_fifo #(byte unsigned)    ingress_ll_to_tl_pkt_fifo; 
  uvm_tlm_fifo #(int)              ingress_ll_to_tl_cntrl_fifo; 
  uvm_tlm_fifo #(byte unsigned)    ingress_pl_to_tl_pkt_fifo; 
  uvm_tlm_fifo #(int)              ingress_pl_to_tl_cntrl_fifo; 
  rapidio_tl_driver rapidio_tl_drvr;
  rapidio_tl_sequencer rapidio_tl_seqr;
  rapidio_tl_monitor rapidio_tl_mon;
 
  // UVM automation macros
  `uvm_component_utils_begin(rapidio_tl_agent)
    `uvm_field_object(rapidio_tl_drvr, UVM_ALL_ON)
    `uvm_field_object(rapidio_tl_seqr, UVM_ALL_ON)
    `uvm_field_object(m_config, UVM_ALL_ON)
    `uvm_field_object(rap_tl_adp, UVM_ALL_ON)
  `uvm_component_utils_end

  // Standard UVM methods
  extern function new(string name = "rapidio_tl_agent", uvm_component parent=null); 
  extern function void build_phase(uvm_phase phase);
  extern function void connect_phase(uvm_phase phase);
  
  bit                       is_active = 1'b1;
  virtual interface         rapidio_interface  vif;

  endclass: rapidio_tl_agent

// FUNC : new - constructor
function rapidio_tl_agent::new(string name = "rapidio_tl_agent", uvm_component parent=null);
    super.new(name, parent);
    tl_cov_port = new("tl_cov_port",this);
    tl_scbd_port = new("tl_scbd_port",this);
    ingress_ll_to_tl_pkt_fifo = new("ingress_ll_to_tl_pkt_fifo",this,0);
    ingress_ll_to_tl_cntrl_fifo = new("ingress_ll_to_tl_cntrl_fifo",this,0);
    ingress_pl_to_tl_pkt_fifo = new("ingress_pl_to_tl_pkt_fifo",this,0);
    ingress_pl_to_tl_cntrl_fifo = new("ingress_pl_to_tl_cntrl_fifo",this,0);
endfunction

// FUNC : build_phase
function void rapidio_tl_agent::build_phase(uvm_phase phase);
  super.build_phase(phase);
  if(!uvm_config_db#(virtual rapidio_interface)::get(this, "", "vif", vif))
    `uvm_fatal("RAPIDIO_TL_AGNT_FATAL",{"VIRTUAL INTERFACE MUST BE SET FOR: ",get_full_name(),".vif"});
  if(is_active)  begin
    rapidio_tl_drvr = rapidio_tl_driver::type_id::create("rapidio_tl_drvr",this);
    rap_tl_adp = rapidio_adapter::type_id::create("rap_tl_adp",this);
    rapidio_tl_seqr = rapidio_tl_sequencer::type_id::create("rapidio_tl_seqr",this);
    rapidio_tl_mon = rapidio_tl_monitor::type_id::create("rapidio_tl_mon",this);
  end
endfunction  
 
// FUNC : connect_phase
function void rapidio_tl_agent::connect_phase(uvm_phase phase);
super.connect_phase(phase);
  if(is_active == 1) 
begin
    rapidio_tl_drvr.seq_item_port.connect(rapidio_tl_seqr.seq_item_export);
  end  
   rapidio_tl_drvr.get_ll_pkt_port.connect(ingress_ll_to_tl_pkt_fifo.get_export);	
   rapidio_tl_drvr.get_ll_cntrl_port.connect(ingress_ll_to_tl_cntrl_fifo.get_export);	
   rapidio_tl_mon.get_pl_pkt_port.connect(ingress_pl_to_tl_pkt_fifo.get_export);	
   rapidio_tl_mon.get_pl_cntrl_port.connect(ingress_pl_to_tl_cntrl_fifo.get_export);	
endfunction
   

`endif
