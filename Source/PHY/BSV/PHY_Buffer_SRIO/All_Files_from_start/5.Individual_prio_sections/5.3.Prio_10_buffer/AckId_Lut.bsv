package AckId_Lut;

import Vector::*;


interface Ifc_AckId_Lut;
method Action _ackid_store(Bit#(6) value);
method Action _ackid_retrans(Bit#(6) value);
method Action _prio_in(Bit#(2) value);
method Action _rd_ptr_in(Bit#(4) value);
method Action _identify(Bit#(2) value);

method Bit#(2) prio_out_();
method Bit#(4) read_ptr_out_();
method Bit#(6) ackid_out_();
endinterface:Ifc_AckId_Lut

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkAckId_Lut(Ifc_AckId_Lut);


//[6-bit ackid,2-bit prio,4-bit pointr postn]
Vector#(64,Reg#(Bit#(12)))  rg_vect_lut <- replicateM(mkReg(0));

//ackid to be stored while reading
Wire#(Bit#(6)) wr_ack_id_store <- mkDWire(0);
//ackid to be retransmitted
Wire#(Bit#(6)) wr_ack_id_retrans <- mkDWire(0);
Wire#(Bit#(4)) wr_rd_ptr_in <- mkDWire(0);
Wire#(Bit#(2)) wr_prio <- mkDWire(0);
//to indicate whether updation or reading of lut is required
Wire#(Bit#(2)) id <- mkDWire(0);

Reg#(Bit#(4)) rg_read_ptr_out <- mkReg(0);
Reg#(Bit#(2)) rg_prio_out <- mkReg(0);
Reg#(Bit#(6)) rg_ack_id_retrans <- mkReg(0);




rule update(id == 2'b01);
let lv_id = wr_ack_id_store;
//vector of registers indexed by ackid
rg_vect_lut[lv_id] <= {wr_ack_id_store,wr_prio,wr_rd_ptr_in};
$display("LUT entry:Ackid - %b,Priority - %b,Pointer - %b",wr_ack_id_store,wr_prio,wr_rd_ptr_in);
endrule

//reading from lut
rule out_retrans(id == 2'b10);
let lv_id = wr_ack_id_retrans;
rg_read_ptr_out <= rg_vect_lut[lv_id][3:0];
rg_prio_out <= rg_vect_lut[lv_id][5:4];
rg_ack_id_retrans <= wr_ack_id_retrans;
endrule


//ackid to be stored
method Action _ackid_store(Bit#(6) value);
       wr_ack_id_store <= value;
endmethod

//ackid to be identified from lut for retransmission
method Action _ackid_retrans(Bit#(6) value);
       wr_ack_id_retrans <= value;
endmethod

method Action _rd_ptr_in(Bit#(4) value);
       wr_rd_ptr_in <= value;
endmethod

method Action _prio_in(Bit#(2) value);
	wr_prio <= value;
endmethod
	
method Action _identify(Bit#(2) value);
       id <= value;
endmethod

method Bit#(4) read_ptr_out_();
       return rg_read_ptr_out;
endmethod

method Bit#(2) prio_out_();
	return rg_prio_out;
endmethod

method Bit#(6) ackid_out_();
	return rg_ack_id_retrans;
endmethod


endmodule:mkAckId_Lut
endpackage:AckId_Lut
