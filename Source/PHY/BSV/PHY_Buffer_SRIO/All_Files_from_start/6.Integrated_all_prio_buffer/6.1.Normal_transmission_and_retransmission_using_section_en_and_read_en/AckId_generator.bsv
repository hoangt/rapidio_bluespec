package AckId_generator;

interface Ifc_AckId_generator;
	method Action _ack_en(Bool value);
	method Action _ack_in(Bit#(6) value);
	method Action _retrans(Bit#(1) value);

	method Bit#(6) ack_id_out_();
endinterface:Ifc_AckId_generator

(* synthesize *)
(* always_enabled *)
(* always_ready *)

//In case retransmssn is asked for,at the same time as when ackid is to be incremented,retransmissn must be given preference.
(* descending_urgency = "retrns,ak_id_incr" *)

module mkAckId_generator(Ifc_AckId_generator);

//initial value of ackid on reset - zero
Reg#(Bit#(12)) rg_new_ackid <- mkReg(12'b0); 
                                
Wire#(Bool) wr_enbl <- mkDWire(True);
Wire#(Bit#(6)) wr_ackid_in <- mkDWire(0);
Wire#(Bit#(1)) wr_retrans <- mkDWire(0);


rule ak_id_incr(wr_enbl == False);
//maximum value of ackid,which is 11 bits long.after that it should roll over to initial value
	if(rg_new_ackid == 12'hFFF)             
		rg_new_ackid <= 12'b0;
	else
//ackid sequential in nature;spec part 6 - section 2.4.
		rg_new_ackid <= rg_new_ackid + 1;  
endrule



rule retrns(wr_retrans == 1);
let lv_rg_new_ackid = rg_new_ackid;
//ackid msbs from control symbol to be given instead of lv_rg if required
rg_new_ackid <= {lv_rg_new_ackid[11:6],wr_ackid_in};
endrule



method Action _ack_in(Bit#(6) value);
	wr_ackid_in <=value;
endmethod

method Action _ack_en(Bool value);
	wr_enbl <= value;
endmethod

method Action _retrans(Bit#(1) value);
	wr_retrans <= value;
endmethod

method Bit#(6) ack_id_out_();
      return rg_new_ackid[5:0];
endmethod

endmodule:mkAckId_generator
endpackage:AckId_generator
