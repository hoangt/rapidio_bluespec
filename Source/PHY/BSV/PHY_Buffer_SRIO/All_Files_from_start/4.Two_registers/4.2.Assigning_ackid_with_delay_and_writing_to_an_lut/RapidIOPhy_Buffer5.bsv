package RapidIOPhy_Buffer5;

import RapidIO_DTypes ::*;
//import AckId_generator::*;
//import AckId_Lut::*;

interface Ifc_RapidIOPhy_Buffer5;
   method Action _tx_sof_n(Bool value);
   method Action _tx_eof_n(Bool value);
   method Action _tx_vld_n(Bool value);
   //method Action _rd_ptr_in(Bit#(2) value);
   method Action _tx_data(DataPkt value);
   method Action _tx_rem(Bit#(4) value);
   method Action _tx_crf(Bit#(2) value);
   method Action _tx_read(Bit#(1) value);
   method Action _tx_deq(Bit#(1) value);
   method Action _tx_ack(Bit#(2) value);
   //method Action _rd_ptr_in(Bit#(2) value);

   
   method Bool lnk_tvld_n_();
   method Bool lnk_tsof_n_();
   method Bool lnk_teof_n_();
   method DataPkt lnk_td_();
   method Bit#(4) lnk_trem_();
   method Bit#(2) lnk_tcrf_();
   method RegBuf buf_out_();


endinterface:Ifc_RapidIOPhy_Buffer5



(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkRapidIOPhy_Buffer5(Ifc_RapidIOPhy_Buffer5);

//Ifc_AckId_Lut a2 <- mkAckId_Lut;
//Ifc_AckId_generator a1 <- mkAckId_generator;

Wire#(Bool) tx_sof_n <- mkDWire(True);
Wire#(Bool) tx_eof_n <- mkDWire(True);
Wire#(Bool) tx_vld_n <- mkDWire(True);
//Wire#(Bool) tx_rdy_n <- mkDWire(True);
Wire#(DataPkt) tx_data <- mkDWire(0);
Wire#(Bit#(4)) tx_rem <- mkDWire(0);
Wire#(Bit#(2)) tx_crf <- mkDWire(0);

Wire#(Bool) lnk_tvld_n <- mkDWire(True);
Wire#(Bool) lnk_tsof_n <- mkDWire(True);
Wire#(Bool) lnk_teof_n <- mkDWire(True);
Wire#(DataPkt) lnk_td <- mkDWire(0);
Wire#(Bit#(4)) lnk_trem <- mkDWire(0);
Wire#(Bit#(2)) lnk_tcrf <- mkDWire(0);
//Wire#(Bit#(2)) rd_ptr_in <- mkDWire(0);
Wire#(Bit#(2)) tx_ack <- mkDWire(0);


Reg#(RegBuf) rg_buf <- mkReg(0);
Reg#(Bit#(5)) rg_cnt_rd <- mkReg(0);
Reg#(Bit#(5)) rg_cnt_wr <- mkReg(0);//for storing rg_cnt value
Reg#(Bit#(1)) rg_read <- mkReg(0);// reg for write to read ie making both of them mutually exclusive
Wire#(Bit#(1)) wr_deq <- mkDWire(0);//reg for deciding about dequeue.
//Reg#(Bit#(2)) rg_cnt_ackid <- mkReg(0);



//rule r1;
//a <= rg_cnt;
//rg_cnt <= rg_cnt + 128;
//endrule

//rule r1_deq(rg_deq == 1);
//rg_buf[2311:0] <= 2312'b0;
//endrule

rule r1_write(tx_vld_n == False || wr_deq == 1); 
//&& rg_buf[2305] == 1);
//Bit#(8) a = rg_cnt;
  
if(wr_deq == 1)
rg_buf[2311:0] <= 2312'b0;
else if(tx_sof_n == False && tx_eof_n == False)
begin            
rg_buf[2311:0] <= {1'b1,tx_crf,tx_rem,1'b1,2176'b0,tx_data};
//rg_read <= 1'b1;
//out[2311:0] <= rg_buf[2311:0];
//$display("Reg value is %b",out[2311:0]);
rg_cnt_wr <= rg_cnt_wr +1;

end
//$display ("rg_buf[2311] == %b", rg_buf[2311]);
else if(tx_sof_n == False)
begin            
rg_buf[127:0] <= tx_data;
rg_cnt_wr <= rg_cnt_wr +2;
//rg_cnt_rd <= rg_cnt_rd +1;
end

else if(tx_eof_n == True && tx_sof_n ==True)
begin
if(rg_cnt_wr == 2)
begin
rg_buf[255:128] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

/*if(rg_cnt_wr ==2)
begin
rg_buf[255:128] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end*/

if(rg_cnt_wr == 3)
begin
rg_buf[383:256] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 4)
begin
rg_buf[511:384] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 5)
begin
rg_buf[639:512] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 6)
begin
rg_buf[767:640] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 7)
begin
rg_buf[895:768] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 8)
begin
rg_buf[1023:896] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 9)
begin
rg_buf[1151:1024] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 10)
begin
rg_buf[1279:1152] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 11)
begin
rg_buf[1407:1280] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 12)
begin
rg_buf[1535:1408] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 13)
begin
rg_buf[1663:1536] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 14)
begin
rg_buf[1791:1664] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 15)
begin
rg_buf[1919:1792] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 16)
begin
rg_buf[2047:1920] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end

if(rg_cnt_wr == 17)
begin
rg_buf[2175:2048] <= tx_data;
rg_cnt_wr <= rg_cnt_wr+1;
end





end


else if(tx_eof_n == False)
begin

rg_buf[(2311):(2176)] <= {1'b1,tx_crf,tx_rem,1'b1,tx_data};
//rg_cnt1 <= rg_cnt;
rg_cnt_rd <= 0;
//rg_read <= 1'b1;
//out[2311:0] <= rg_buf[2311:0];
//$display("Reg value is %b",rg_buf[2311:2304]);
end
//tx_last_data <=tx_data;  
      
//display ("rg_buf[2311] == %b", rg_buf[2311]); 
endrule

/*rule r1_stallread(tx_vld_n == True );
rg_read <= 1'b1;

//rule r1_stallrtw(tx_vld_n == False && rg_read == 1);
//rg_read <= 1'b0;
//endrule
*/

rule r1_read(rg_read == 1);
//if(tx_vld_n == True)
//rg_buf[2305] <= 0;
//Bit#(8) a = rg_cnt;
if(rg_cnt_wr <= 1 && rg_cnt_rd == 0)
begin
if(rg_cnt_wr == 1)
begin
rg_buf[1:0] <= tx_ack;
$display("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
rg_cnt_wr <= rg_cnt_wr-1;
//a2._identify(2'b01);
//a2._rd_ptr_in(rd_ptr_in);
//a2._ackid(tx_ack);
end
else if(rg_cnt_wr == 0)
begin
lnk_tsof_n <= False;
lnk_teof_n <= False;
lnk_tvld_n <= False;
lnk_td <= rg_buf[127:0];
lnk_trem <= rg_buf[2308:2305];
lnk_tcrf <= rg_buf[2310:2309];
//a1._ackid(rg_cnt_ackid);
//a1._rd_ptr_in()
//rg_buf[2304] <= 1'b0;
//rg_buf[2311:2304] <= 8'b0;
//rg_read <= 1'b0;
//rg_cnt <= rg_cnt - 1;
end
end

else if(rg_cnt_wr >= 1)

begin
if(rg_cnt_rd == 0)
begin
rg_buf[1:0] <= tx_ack;
$display("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
rg_cnt_wr <= rg_cnt_wr-1;
rg_cnt_rd <= rg_cnt_rd + 1;
//$display("fire");
//a2._identify(2'b01);
//a2._rd_ptr_in(rd_ptr_in);
//a2._ackid(tx_ack);

end
/*else if(rg_cnt_wr > 1 && rg_cnt_rd == 1)
begin
$display("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
rg_buf[1:0] <= tx_ack;
//a2._identify(2'b01);
//a2._rd_ptr_in(rd_ptr_in);
//a2._ackid(tx_ack);

rg_cnt_wr <= rg_cnt_wr-1;
rg_cnt_rd <= rg_cnt_rd + 1;

end*/
else if(rg_cnt_rd == 1)
begin
lnk_tsof_n <= False;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[127:0];
//lnk_trem <= rg_buf[2308:2305];
//lnk_tcrf <= rg_buf[2310:2309];
//rg_buf[2304] <= 1'b0;
rg_cnt_rd <= rg_cnt_rd + 1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

//else if(rg_buf[2304] == 1'b0 && rg_buf[2311] == 1'b1 && rg_cnt1 != 0)
//begin
else if(rg_cnt_rd == 2)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[255:128];
rg_cnt_rd <= rg_cnt_rd + 1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 3)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[383:256];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 4)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[511:384];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 5)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[639:512];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 6)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[767:640];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 7)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[895:768];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 8)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1023:896];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 9)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1151:1024];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 10)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1279:1152];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 11)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1407:1280];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 12)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1535:1408] ;
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 13)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1663:1536];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 14)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1791:1664];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 15)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[1919:1792];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 16)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[2047:1920];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end

else if(rg_cnt_rd == 17)
begin
lnk_tsof_n <= True;
lnk_teof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[2175:2048];
rg_cnt_rd <= rg_cnt_rd+1;
rg_cnt_wr <= rg_cnt_wr - 1;
end





end


else if(rg_cnt_wr == 0 && rg_cnt_rd != 0)
begin
lnk_teof_n <= False;
lnk_tsof_n <= True;
lnk_tvld_n <= False;
lnk_td <= rg_buf[2303:2176];
//rg_buf[2311] <= 1'b0;
lnk_trem <= rg_buf[2308:2305];
lnk_tcrf <= rg_buf[2310:2309];
rg_cnt_rd <= 0;
//rg_read <= 1'b0;
//out[2311:0] <= rg_buf[2311:0];
//$display("Reg value is %b",out[2311:0]);
end
//tx_last_data <=tx_data;  
      



endrule


method Action _tx_ack(Bit#(2) value);
	tx_ack <= value;
endmethod

method Action _tx_sof_n(Bool value);
     tx_sof_n <= value;
endmethod

method Action _tx_eof_n(Bool value);
     tx_eof_n <= value;
endmethod

method Action _tx_vld_n(Bool value);
     tx_vld_n <= value;
endmethod

method Action _tx_data(DataPkt value);
     tx_data <= value;
endmethod

method Action _tx_rem(Bit#(4) value);
     tx_rem <= value;
endmethod

method Action _tx_crf(Bit#(2) value);
     tx_crf <= value;
endmethod

method Action _tx_read(Bit#(1) value);
     rg_read <= value;
endmethod

method Action _tx_deq(Bit#(1) value);
     wr_deq <= value;
endmethod

/*method Action _rd_ptr_in(Bit#(2) value);
     rd_ptr_in <= value;
endmethod*/

method Bool lnk_tvld_n_();
     return lnk_tvld_n;
endmethod

method RegBuf buf_out_();
     return rg_buf;
endmethod

method Bool lnk_tsof_n_();
     return lnk_tsof_n;
endmethod

method Bool lnk_teof_n_();
      return lnk_teof_n;
endmethod

method DataPkt lnk_td_();
      return lnk_td;
endmethod

method Bit#(4) lnk_trem_();
      return lnk_trem;
endmethod

method Bit#(2) lnk_tcrf_();
      return lnk_tcrf;
endmethod

endmodule:mkRapidIOPhy_Buffer5
endpackage:RapidIOPhy_Buffer5
