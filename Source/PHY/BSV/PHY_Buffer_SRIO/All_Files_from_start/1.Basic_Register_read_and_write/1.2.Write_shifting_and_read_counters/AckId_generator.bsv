package AckId_generator;

interface Ifc_AckId_generator;
	method Action _ack_en(Bool value);
	method Bit#(12) ack_id_();
endinterface:Ifc_AckId_generator

(* synthesize *)
(* always_enabled *)
(* always_ready *)

module mkAckId_generator(Ifc_AckId_generator);

Reg#(Bit#(12)) rg_new_ackid <- mkReg(12'hFFF);
Wire#(Bool) wr_enbl <- mkDWire(True);

rule r8(wr_enbl == False);
	if(rg_new_ackid == 12'hFFF)
		rg_new_ackid <= 12'b0;
	else
		rg_new_ackid <= rg_new_ackid + 1;
endrule

method Action _ack_en(Bool value);
	wr_enbl <= value;
endmethod

method Bit#(12) ack_id_();
      return rg_new_ackid;
endmethod

endmodule:mkAckId_generator
endpackage:AckId_generator
