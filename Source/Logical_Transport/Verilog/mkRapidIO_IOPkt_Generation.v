/*
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- RapidIO IO Packet Generation Module IP Core
--
-- This file is the part of the RapidIO Interconnect v3.0 IP Core Project
--
-- Description
-- This Module developed, 
-- 1. To generate the Ftype Packets. 
-- 2. It invokes TxFtypeFunctions Package to generate ftype Header and Data packets
-- 3. The packet is generated with the latency of 1 cycle. 
-- 4. Ftype Packet Header information is stored until the transaction is completed. 
-- 5. Used temporary registers to store the data for packet generation
-- 6. The Packet is generated using the Ftype field and SOF determines the Header packet.
-- 7. Byte Count value is decremented by 16 for every 16 bytes sent and if it is less 
--    than 16, the eof is asserted. 
-- 8. Output SOF, EOF, Vld and TxRem are determined.  
-- 9. The Packet information is shown below. 
-- 10. Bus Interface Ready-Valid operation is implemented. 
--
-- To do's
-- 1. GSM Implementation
-- 2. Flow Control 
-- 3. Data Streaming
--
--
-- Author(s):
-- Chidhambaranathan (cnaathan@gmail.com)
--
--------------------------------------------------------------------------------------------------------------------------------------------------------
-- 
-- Copyright (c) 2013, Indian Institute of Technology Madras (IIT Madras)
-- All rights reserved.
--
-- Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
--
-- 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
-- 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and 
--    the following disclaimer in the documentation and/or other materials provided with the distribution.
-- 3. Neither the name of IIT Madras  nor the names of its contributors may be used to endorse or 
--    promote products derived from this software without specific prior written permission.
--
-- THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, 
-- INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
-- IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, 
-- OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; 
-- OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, 
-- OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
-- 
--------------------------------------------------------------------------------------------------------------------------------------------------------


	-- Packet Information -- 

1. Request Class  (Atomic or NREAD Request)
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Read Size[3:0], Source TranID[7:0], Address[28:0], WdPtr, xamsbs[1:0], Resv[55:0]}

2. Write Request  (Atomic Write or NWRITE Request)
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Write Size[3:0], Source TranID[7:0], Address[28:0], WdPtr, xamsbs[1:0], Data[63:8]}
	Data_1 -> { Data[7:0], Resv[55:0]}

3. Stream Write Class 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Address[28:0], Resv, Xamsbs[1;0], Data1, 8'h00 }
	Data_2 -> { Data2, 64'h0 }
	Data_3 -> { Data3, 64'h0 } 	// For 3 Stream Data, But the Data Stream can increase 

4. Maintenance Read Request 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Read Size[3:0], Target TranID[7:0], Hop_Count[7:0], Offset[20:0], WdPtr, Resv }

5. Maintenance Write Request 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Write Size[3:0], Target TranID[7:0], Hop_Count[7:0], Offset[20:0], WdPtr, Resv, Data[63:8] }
	Data_1 -> { Data[7:0] }

6. Maintenance Read Response 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0], Hop_Count[7:0], Resv[15:0], Data[63:8] }
	Data_1 -> { Data[7:0] }

7. Maintenance Write Response 
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0], Hop_Count[7:0], Resv[15:0] }

8. Response With Data
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0], Data[63:0] }

9. Response Without Data
	Header -> { Priority[1:0], Tt[1:0], Ftype[3:0], Destination ID[7:0], Source ID[7:0], Ttype[3:0], Status[3:0], Target TranID[7:0] }

*/

//
// Generated by Bluespec Compiler, version 2013.05.beta2 (build 31258, 2013-05-21)
//
// On Mon Apr 21 12:27:08 IST 2014
//
// Method conflict info:
// Method: _inputs_Ftype2IOReqClass
// Conflict-free: _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_Ftype2IOReqClass
//
// Method: _inputs_Ftype5IOWrClass
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_Ftype5IOWrClass
//
// Method: _inputs_Ftype6IOStreamClass
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Sequenced before (restricted): _inputs_InitReqDataCount
// Conflicts: _inputs_Ftype6IOStreamClass
//
// Method: _inputs_Ftype8IOMaintenanceClass
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_Ftype8IOMaintenanceClass
//
// Method: _inputs_Ftype9DataStreamingClass
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_Ftype9DataStreamingClass
//
// Method: _inputs_Ftype10MgDOORBELLClass
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_Ftype10MgDOORBELLClass
//
// Method: _inputs_Ftype11MESSAGEClass
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_Ftype11MESSAGEClass
//
// Method: _inputs_Ftype13IORespClass
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_Ftype13IORespClass
//
// Method: _inputs_InitReqIfcPkt
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Sequenced before (restricted): _inputs_InitReqDataCount
// Conflicts: _inputs_InitReqIfcPkt
//
// Method: _inputs_TgtRespIfcPkt
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_TgtRespIfcPkt
//
// Method: _inputs_MaintenanceRespIfcPkt
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Conflicts: _inputs_MaintenanceRespIfcPkt
//
// Method: _inputs_InitReqDataCount
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Sequenced after (restricted): _inputs_Ftype6IOStreamClass,
// 			      _inputs_InitReqIfcPkt
// Conflicts: _inputs_InitReqDataCount
//
// Method: pkgen_sof_n_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
//
// Method: pkgen_eof_n_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
//
// Method: pkgen_vld_n_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
//
// Method: pkgen_dsc_n_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
//
// Method: pkgen_rdy_n
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_
// Sequenced before (restricted): outputs_RxRdy_From_Dest_
// Conflicts: pkgen_rdy_n
//
// Method: pkgen_data_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
//
// Method: pkgen_tx_rem_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
//
// Method: pkgen_crf_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_rdy_n,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
//
// Method: outputs_RxRdy_From_Dest_
// Conflict-free: _inputs_Ftype2IOReqClass,
// 	       _inputs_Ftype5IOWrClass,
// 	       _inputs_Ftype6IOStreamClass,
// 	       _inputs_Ftype8IOMaintenanceClass,
// 	       _inputs_Ftype9DataStreamingClass,
// 	       _inputs_Ftype10MgDOORBELLClass,
// 	       _inputs_Ftype11MESSAGEClass,
// 	       _inputs_Ftype13IORespClass,
// 	       _inputs_InitReqIfcPkt,
// 	       _inputs_TgtRespIfcPkt,
// 	       _inputs_MaintenanceRespIfcPkt,
// 	       _inputs_InitReqDataCount,
// 	       pkgen_sof_n_,
// 	       pkgen_eof_n_,
// 	       pkgen_vld_n_,
// 	       pkgen_dsc_n_,
// 	       pkgen_data_,
// 	       pkgen_tx_rem_,
// 	       pkgen_crf_,
// 	       outputs_RxRdy_From_Dest_
// Sequenced after (restricted): pkgen_rdy_n
//
//
// Ports:
// Name                         I/O  size props
// pkgen_sof_n_                   O     1
// pkgen_eof_n_                   O     1
// pkgen_vld_n_                   O     1
// pkgen_dsc_n_                   O     1
// pkgen_data_                    O   128
// pkgen_tx_rem_                  O     4
// pkgen_crf_                     O     1 const
// outputs_RxRdy_From_Dest_       O     1
// CLK                            I     1 clock
// RST_N                          I     1 reset
// _inputs_Ftype2IOReqClass_pkt   I    70
// _inputs_Ftype5IOWrClass_pkt    I   135
// _inputs_Ftype6IOStreamClass_pkt  I    53
// _inputs_Ftype8IOMaintenanceClass_pkt  I   109
// _inputs_Ftype9DataStreamingClass_pkt  I   100 unused
// _inputs_Ftype10MgDOORBELLClass_pkt  I    30
// _inputs_Ftype11MESSAGEClass_pkt  I    30
// _inputs_Ftype13IORespClass_pkt  I    87
// _inputs_InitReqIfcPkt_ireqpkt  I   229
// _inputs_TgtRespIfcPkt_tgtresppkt  I   134
// _inputs_MaintenanceRespIfcPkt_mresppkt  I   133
// _inputs_InitReqDataCount_value  I     5 reg
// pkgen_rdy_n_value              I     1
//
// Combinational paths from inputs to outputs:
//   pkgen_rdy_n_value -> outputs_RxRdy_From_Dest_
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module mkRapidIO_IOPkt_Generation(CLK,
				  RST_N,

				  _inputs_Ftype2IOReqClass_pkt,

				  _inputs_Ftype5IOWrClass_pkt,

				  _inputs_Ftype6IOStreamClass_pkt,

				  _inputs_Ftype8IOMaintenanceClass_pkt,

				  _inputs_Ftype9DataStreamingClass_pkt,

				  _inputs_Ftype10MgDOORBELLClass_pkt,

				  _inputs_Ftype11MESSAGEClass_pkt,

				  _inputs_Ftype13IORespClass_pkt,

				  _inputs_InitReqIfcPkt_ireqpkt,

				  _inputs_TgtRespIfcPkt_tgtresppkt,

				  _inputs_MaintenanceRespIfcPkt_mresppkt,

				  _inputs_InitReqDataCount_value,

				  pkgen_sof_n_,

				  pkgen_eof_n_,

				  pkgen_vld_n_,

				  pkgen_dsc_n_,

				  pkgen_rdy_n_value,

				  pkgen_data_,

				  pkgen_tx_rem_,

				  pkgen_crf_,

				  outputs_RxRdy_From_Dest_);
  input  CLK;
  input  RST_N;

  // action method _inputs_Ftype2IOReqClass
  input  [69 : 0] _inputs_Ftype2IOReqClass_pkt;

  // action method _inputs_Ftype5IOWrClass
  input  [134 : 0] _inputs_Ftype5IOWrClass_pkt;

  // action method _inputs_Ftype6IOStreamClass
  input  [52 : 0] _inputs_Ftype6IOStreamClass_pkt;

  // action method _inputs_Ftype8IOMaintenanceClass
  input  [108 : 0] _inputs_Ftype8IOMaintenanceClass_pkt;

  // action method _inputs_Ftype9DataStreamingClass
  input  [99 : 0] _inputs_Ftype9DataStreamingClass_pkt;

  // action method _inputs_Ftype10MgDOORBELLClass
  input  [29 : 0] _inputs_Ftype10MgDOORBELLClass_pkt;

  // action method _inputs_Ftype11MESSAGEClass
  input  [29 : 0] _inputs_Ftype11MESSAGEClass_pkt;

  // action method _inputs_Ftype13IORespClass
  input  [86 : 0] _inputs_Ftype13IORespClass_pkt;

  // action method _inputs_InitReqIfcPkt
  input  [228 : 0] _inputs_InitReqIfcPkt_ireqpkt;

  // action method _inputs_TgtRespIfcPkt
  input  [133 : 0] _inputs_TgtRespIfcPkt_tgtresppkt;

  // action method _inputs_MaintenanceRespIfcPkt
  input  [132 : 0] _inputs_MaintenanceRespIfcPkt_mresppkt;

  // action method _inputs_InitReqDataCount
  input  [4 : 0] _inputs_InitReqDataCount_value;

  // value method pkgen_sof_n_
  output pkgen_sof_n_;

  // value method pkgen_eof_n_
  output pkgen_eof_n_;

  // value method pkgen_vld_n_
  output pkgen_vld_n_;

  // value method pkgen_dsc_n_
  output pkgen_dsc_n_;

  // action method pkgen_rdy_n
  input  pkgen_rdy_n_value;

  // value method pkgen_data_
  output [127 : 0] pkgen_data_;

  // value method pkgen_tx_rem_
  output [3 : 0] pkgen_tx_rem_;

  // value method pkgen_crf_
  output pkgen_crf_;

  // value method outputs_RxRdy_From_Dest_
  output outputs_RxRdy_From_Dest_;

  // signals for module outputs
  wire [127 : 0] pkgen_data_;
  wire [3 : 0] pkgen_tx_rem_;
  wire outputs_RxRdy_From_Dest_,
       pkgen_crf_,
       pkgen_dsc_n_,
       pkgen_eof_n_,
       pkgen_sof_n_,
       pkgen_vld_n_;

  // inlined wires
  wire [135 : 0] ff_TransmitPktIfcFIFO_enqw$wget,
		 wr_PktGenTranmitIfcFirst$wget;
  wire ff_TransmitPktIfcFIFO_enqw$whas,
       wr_EOF_Ftype11Vld$wget,
       wr_EOF_Ftype13Vld$wget,
       wr_EOF_Ftype6Vld$whas,
       wr_EOF_Ftype8Vld$wget,
       wr_EOF_Ftype8Vld$whas,
       wr_OutputFtype10DataPkt$whas,
       wr_OutputFtype11DataPkt$whas,
       wr_OutputFtype11Vld$wget,
       wr_OutputFtype13DataPkt$whas,
       wr_OutputFtype2DataPkt$whas,
       wr_OutputFtype5DataPkt$whas,
       wr_OutputFtype6DataPkt$whas,
       wr_OutputFtype8MRespDataPkt$whas,
       wr_OutputFtype8Vld$whas,
       wr_SOF_Ftype13Vld$wget,
       wr_SOF_Ftype8Vld$wget,
       wr_SOF_Ftype8Vld$whas,
       wr_TxRem_Ftype11$whas,
       wr_TxRem_Ftype5$whas,
       wr_TxRem_Ftype8$whas;

  // register pkgen_DSC_n
  reg pkgen_DSC_n;
  wire pkgen_DSC_n$D_IN, pkgen_DSC_n$EN;

  // register pkgen_EOF_n
  reg pkgen_EOF_n;
  wire pkgen_EOF_n$D_IN, pkgen_EOF_n$EN;

  // register pkgen_SOF_n
  reg pkgen_SOF_n;
  wire pkgen_SOF_n$D_IN, pkgen_SOF_n$EN;

  // register pkgen_VLD_n
  reg pkgen_VLD_n;
  wire pkgen_VLD_n$D_IN, pkgen_VLD_n$EN;

  // register rg_CurrentInitReqByteCount
  reg [8 : 0] rg_CurrentInitReqByteCount;
  wire [8 : 0] rg_CurrentInitReqByteCount$D_IN;
  wire rg_CurrentInitReqByteCount$EN;

  // register rg_CurrentMaintainRespByteCount
  reg [8 : 0] rg_CurrentMaintainRespByteCount;
  wire [8 : 0] rg_CurrentMaintainRespByteCount$D_IN;
  wire rg_CurrentMaintainRespByteCount$EN;

  // register rg_CurrentTgtRespByteCount
  reg [8 : 0] rg_CurrentTgtRespByteCount;
  wire [8 : 0] rg_CurrentTgtRespByteCount$D_IN;
  wire rg_CurrentTgtRespByteCount$EN;

  // register rg_DataResponseValid
  reg rg_DataResponseValid;
  wire rg_DataResponseValid$D_IN, rg_DataResponseValid$EN;

  // register rg_Ftype10DoorbellCsInputDelay
  reg [29 : 0] rg_Ftype10DoorbellCsInputDelay;
  wire [29 : 0] rg_Ftype10DoorbellCsInputDelay$D_IN;
  wire rg_Ftype10DoorbellCsInputDelay$EN;

  // register rg_Ftype10InputValid
  reg [29 : 0] rg_Ftype10InputValid;
  wire [29 : 0] rg_Ftype10InputValid$D_IN;
  wire rg_Ftype10InputValid$EN;

  // register rg_Ftype11DataValid
  reg [63 : 0] rg_Ftype11DataValid;
  wire [63 : 0] rg_Ftype11DataValid$D_IN;
  wire rg_Ftype11DataValid$EN;

  // register rg_Ftype11DataValidDelayed
  reg [63 : 0] rg_Ftype11DataValidDelayed;
  wire [63 : 0] rg_Ftype11DataValidDelayed$D_IN;
  wire rg_Ftype11DataValidDelayed$EN;

  // register rg_Ftype11InputValid
  reg [29 : 0] rg_Ftype11InputValid;
  wire [29 : 0] rg_Ftype11InputValid$D_IN;
  wire rg_Ftype11InputValid$EN;

  // register rg_Ftype11LastData3Delay
  reg rg_Ftype11LastData3Delay;
  wire rg_Ftype11LastData3Delay$D_IN, rg_Ftype11LastData3Delay$EN;

  // register rg_Ftype11LastDeta2Delay
  reg rg_Ftype11LastDeta2Delay;
  wire rg_Ftype11LastDeta2Delay$D_IN, rg_Ftype11LastDeta2Delay$EN;

  // register rg_Ftype11MsgCsInputDelay
  reg [29 : 0] rg_Ftype11MsgCsInputDelay;
  wire [29 : 0] rg_Ftype11MsgCsInputDelay$D_IN;
  wire rg_Ftype11MsgCsInputDelay$EN;

  // register rg_Ftype11_HdrNotComplete
  reg rg_Ftype11_HdrNotComplete;
  wire rg_Ftype11_HdrNotComplete$D_IN, rg_Ftype11_HdrNotComplete$EN;

  // register rg_Ftype13InputValid
  reg [86 : 0] rg_Ftype13InputValid;
  reg [86 : 0] rg_Ftype13InputValid$D_IN;
  wire rg_Ftype13InputValid$EN;

  // register rg_Ftype13RespData
  reg [63 : 0] rg_Ftype13RespData;
  wire [63 : 0] rg_Ftype13RespData$D_IN;
  wire rg_Ftype13RespData$EN;

  // register rg_Ftype13RespInputDelay
  reg [86 : 0] rg_Ftype13RespInputDelay;
  wire [86 : 0] rg_Ftype13RespInputDelay$D_IN;
  wire rg_Ftype13RespInputDelay$EN;

  // register rg_Ftype2InputValid
  reg [69 : 0] rg_Ftype2InputValid;
  wire [69 : 0] rg_Ftype2InputValid$D_IN;
  wire rg_Ftype2InputValid$EN;

  // register rg_Ftype2ReqInputDelay
  reg [69 : 0] rg_Ftype2ReqInputDelay;
  wire [69 : 0] rg_Ftype2ReqInputDelay$D_IN;
  wire rg_Ftype2ReqInputDelay$EN;

  // register rg_Ftype5HeaderNotComplete
  reg rg_Ftype5HeaderNotComplete;
  wire rg_Ftype5HeaderNotComplete$D_IN, rg_Ftype5HeaderNotComplete$EN;

  // register rg_Ftype5InputValid
  reg [134 : 0] rg_Ftype5InputValid;
  wire [134 : 0] rg_Ftype5InputValid$D_IN;
  wire rg_Ftype5InputValid$EN;

  // register rg_Ftype5WrCsInputDelay
  reg [134 : 0] rg_Ftype5WrCsInputDelay;
  wire [134 : 0] rg_Ftype5WrCsInputDelay$D_IN;
  wire rg_Ftype5WrCsInputDelay$EN;

  // register rg_Ftype6DataInput
  reg [63 : 0] rg_Ftype6DataInput;
  wire [63 : 0] rg_Ftype6DataInput$D_IN;
  wire rg_Ftype6DataInput$EN;

  // register rg_Ftype6DataValid
  reg [63 : 0] rg_Ftype6DataValid;
  wire [63 : 0] rg_Ftype6DataValid$D_IN;
  wire rg_Ftype6DataValid$EN;

  // register rg_Ftype6DataValid1Delayed
  reg [63 : 0] rg_Ftype6DataValid1Delayed;
  wire [63 : 0] rg_Ftype6DataValid1Delayed$D_IN;
  wire rg_Ftype6DataValid1Delayed$EN;

  // register rg_Ftype6DataValidDelayed
  reg [63 : 0] rg_Ftype6DataValidDelayed;
  wire [63 : 0] rg_Ftype6DataValidDelayed$D_IN;
  wire rg_Ftype6DataValidDelayed$EN;

  // register rg_Ftype6HeaderNotComplete
  reg rg_Ftype6HeaderNotComplete;
  wire rg_Ftype6HeaderNotComplete$D_IN, rg_Ftype6HeaderNotComplete$EN;

  // register rg_Ftype6InputValid
  reg [52 : 0] rg_Ftype6InputValid;
  wire [52 : 0] rg_Ftype6InputValid$D_IN;
  wire rg_Ftype6InputValid$EN;

  // register rg_Ftype6LastData2Delay
  reg rg_Ftype6LastData2Delay;
  wire rg_Ftype6LastData2Delay$D_IN, rg_Ftype6LastData2Delay$EN;

  // register rg_Ftype6LastData3Delay
  reg rg_Ftype6LastData3Delay;
  wire rg_Ftype6LastData3Delay$D_IN, rg_Ftype6LastData3Delay$EN;

  // register rg_Ftype6LastData4Delay
  reg rg_Ftype6LastData4Delay;
  wire rg_Ftype6LastData4Delay$D_IN, rg_Ftype6LastData4Delay$EN;

  // register rg_Ftype6WrStreamInputDelay
  reg [52 : 0] rg_Ftype6WrStreamInputDelay;
  wire [52 : 0] rg_Ftype6WrStreamInputDelay$D_IN;
  wire rg_Ftype6WrStreamInputDelay$EN;

  // register rg_Ftype8MRespInputDelay
  reg [108 : 0] rg_Ftype8MRespInputDelay;
  wire [108 : 0] rg_Ftype8MRespInputDelay$D_IN;
  wire rg_Ftype8MRespInputDelay$EN;

  // register rg_Ftype8MRespInputValid
  reg [108 : 0] rg_Ftype8MRespInputValid;
  reg [108 : 0] rg_Ftype8MRespInputValid$D_IN;
  wire rg_Ftype8MRespInputValid$EN;

  // register rg_Ftype9_Activate
  reg rg_Ftype9_Activate;
  wire rg_Ftype9_Activate$D_IN, rg_Ftype9_Activate$EN;

  // register rg_InitReqByteCount
  reg [8 : 0] rg_InitReqByteCount;
  wire [8 : 0] rg_InitReqByteCount$D_IN;
  wire rg_InitReqByteCount$EN;

  // register rg_InitReqDataCount
  reg [4 : 0] rg_InitReqDataCount;
  wire [4 : 0] rg_InitReqDataCount$D_IN;
  wire rg_InitReqDataCount$EN;

  // register rg_InitReqInput1Delay
  reg [228 : 0] rg_InitReqInput1Delay;
  wire [228 : 0] rg_InitReqInput1Delay$D_IN;
  wire rg_InitReqInput1Delay$EN;

  // register rg_InitReqInput2Delay
  reg [228 : 0] rg_InitReqInput2Delay;
  wire [228 : 0] rg_InitReqInput2Delay$D_IN;
  wire rg_InitReqInput2Delay$EN;

  // register rg_InitReqInputData
  reg [63 : 0] rg_InitReqInputData;
  wire [63 : 0] rg_InitReqInputData$D_IN;
  wire rg_InitReqInputData$EN;

  // register rg_MRespHeaderNotComplete
  reg rg_MRespHeaderNotComplete;
  wire rg_MRespHeaderNotComplete$D_IN, rg_MRespHeaderNotComplete$EN;

  // register rg_MaintainRespInput1Delay
  reg [132 : 0] rg_MaintainRespInput1Delay;
  wire [132 : 0] rg_MaintainRespInput1Delay$D_IN;
  wire rg_MaintainRespInput1Delay$EN;

  // register rg_MaintainRespInput2Delay
  reg [132 : 0] rg_MaintainRespInput2Delay;
  wire [132 : 0] rg_MaintainRespInput2Delay$D_IN;
  wire rg_MaintainRespInput2Delay$EN;

  // register rg_OutputDataPkt
  reg [127 : 0] rg_OutputDataPkt;
  wire [127 : 0] rg_OutputDataPkt$D_IN;
  wire rg_OutputDataPkt$EN;

  // register rg_TgtRespInput1Delay
  reg [133 : 0] rg_TgtRespInput1Delay;
  wire [133 : 0] rg_TgtRespInput1Delay$D_IN;
  wire rg_TgtRespInput1Delay$EN;

  // register rg_TgtRespInput2Delay
  reg [133 : 0] rg_TgtRespInput2Delay;
  wire [133 : 0] rg_TgtRespInput2Delay$D_IN;
  wire rg_TgtRespInput2Delay$EN;

  // register rg_TxRem
  reg [2 : 0] rg_TxRem;
  wire [2 : 0] rg_TxRem$D_IN;
  wire rg_TxRem$EN;

  // ports of submodule ff_TransmitPktIfcFIFO_ff
  wire [135 : 0] ff_TransmitPktIfcFIFO_ff$D_IN,
		 ff_TransmitPktIfcFIFO_ff$D_OUT;
  wire ff_TransmitPktIfcFIFO_ff$CLR,
       ff_TransmitPktIfcFIFO_ff$DEQ,
       ff_TransmitPktIfcFIFO_ff$EMPTY_N,
       ff_TransmitPktIfcFIFO_ff$ENQ,
       ff_TransmitPktIfcFIFO_ff$FULL_N;

  // ports of submodule ff_TransmitPktIfcFIFO_firstValid
  wire ff_TransmitPktIfcFIFO_firstValid$D_IN,
       ff_TransmitPktIfcFIFO_firstValid$EN,
       ff_TransmitPktIfcFIFO_firstValid$Q_OUT;

  // rule scheduling signals
  wire CAN_FIRE_RL_disp,
       CAN_FIRE_RL_ff_TransmitPktIfcFIFO_dequeue,
       CAN_FIRE_RL_ff_TransmitPktIfcFIFO_enqueue,
       CAN_FIRE_RL_rl_CalculateDSC,
       CAN_FIRE_RL_rl_CurrentInitReqByteCountCalc,
       CAN_FIRE_RL_rl_CurrentMaintenanceRespByteCount,
       CAN_FIRE_RL_rl_CurrentRespByteCount,
       CAN_FIRE_RL_rl_DataPkt_Valid,
       CAN_FIRE_RL_rl_FIFOF_Dequeue,
       CAN_FIRE_RL_rl_FIFOF_First,
       CAN_FIRE_RL_rl_Ftype10Generation,
       CAN_FIRE_RL_rl_Ftype10InputDelay_1Clk,
       CAN_FIRE_RL_rl_Ftype10InputValid,
       CAN_FIRE_RL_rl_Ftype11Generation,
       CAN_FIRE_RL_rl_Ftype11InputDelay_1Clk,
       CAN_FIRE_RL_rl_Ftype13Generation,
       CAN_FIRE_RL_rl_Ftype13InputDelay_1Clk,
       CAN_FIRE_RL_rl_Ftype13InputValid,
       CAN_FIRE_RL_rl_Ftype2Generation,
       CAN_FIRE_RL_rl_Ftype2InputDelay_1Clk,
       CAN_FIRE_RL_rl_Ftype2InputValid,
       CAN_FIRE_RL_rl_Ftype5Generation,
       CAN_FIRE_RL_rl_Ftype5InputDelay_1Clk,
       CAN_FIRE_RL_rl_Ftype5InputValid,
       CAN_FIRE_RL_rl_Ftype6Generation,
       CAN_FIRE_RL_rl_Ftype6InputDelay_1Clk,
       CAN_FIRE_RL_rl_Ftype6InputValid,
       CAN_FIRE_RL_rl_Ftype8Generation,
       CAN_FIRE_RL_rl_Ftype8InputDelay_1Clk,
       CAN_FIRE_RL_rl_Ftype8InputValid,
       CAN_FIRE_RL_rl_InitReqByteCountCalc,
       CAN_FIRE_RL_rl_InitReqInterfaceInput2Delay,
       CAN_FIRE__inputs_Ftype10MgDOORBELLClass,
       CAN_FIRE__inputs_Ftype11MESSAGEClass,
       CAN_FIRE__inputs_Ftype13IORespClass,
       CAN_FIRE__inputs_Ftype2IOReqClass,
       CAN_FIRE__inputs_Ftype5IOWrClass,
       CAN_FIRE__inputs_Ftype6IOStreamClass,
       CAN_FIRE__inputs_Ftype8IOMaintenanceClass,
       CAN_FIRE__inputs_Ftype9DataStreamingClass,
       CAN_FIRE__inputs_InitReqDataCount,
       CAN_FIRE__inputs_InitReqIfcPkt,
       CAN_FIRE__inputs_MaintenanceRespIfcPkt,
       CAN_FIRE__inputs_TgtRespIfcPkt,
       CAN_FIRE_pkgen_rdy_n,
       WILL_FIRE_RL_disp,
       WILL_FIRE_RL_ff_TransmitPktIfcFIFO_dequeue,
       WILL_FIRE_RL_ff_TransmitPktIfcFIFO_enqueue,
       WILL_FIRE_RL_rl_CalculateDSC,
       WILL_FIRE_RL_rl_CurrentInitReqByteCountCalc,
       WILL_FIRE_RL_rl_CurrentMaintenanceRespByteCount,
       WILL_FIRE_RL_rl_CurrentRespByteCount,
       WILL_FIRE_RL_rl_DataPkt_Valid,
       WILL_FIRE_RL_rl_FIFOF_Dequeue,
       WILL_FIRE_RL_rl_FIFOF_First,
       WILL_FIRE_RL_rl_Ftype10Generation,
       WILL_FIRE_RL_rl_Ftype10InputDelay_1Clk,
       WILL_FIRE_RL_rl_Ftype10InputValid,
       WILL_FIRE_RL_rl_Ftype11Generation,
       WILL_FIRE_RL_rl_Ftype11InputDelay_1Clk,
       WILL_FIRE_RL_rl_Ftype13Generation,
       WILL_FIRE_RL_rl_Ftype13InputDelay_1Clk,
       WILL_FIRE_RL_rl_Ftype13InputValid,
       WILL_FIRE_RL_rl_Ftype2Generation,
       WILL_FIRE_RL_rl_Ftype2InputDelay_1Clk,
       WILL_FIRE_RL_rl_Ftype2InputValid,
       WILL_FIRE_RL_rl_Ftype5Generation,
       WILL_FIRE_RL_rl_Ftype5InputDelay_1Clk,
       WILL_FIRE_RL_rl_Ftype5InputValid,
       WILL_FIRE_RL_rl_Ftype6Generation,
       WILL_FIRE_RL_rl_Ftype6InputDelay_1Clk,
       WILL_FIRE_RL_rl_Ftype6InputValid,
       WILL_FIRE_RL_rl_Ftype8Generation,
       WILL_FIRE_RL_rl_Ftype8InputDelay_1Clk,
       WILL_FIRE_RL_rl_Ftype8InputValid,
       WILL_FIRE_RL_rl_InitReqByteCountCalc,
       WILL_FIRE_RL_rl_InitReqInterfaceInput2Delay,
       WILL_FIRE__inputs_Ftype10MgDOORBELLClass,
       WILL_FIRE__inputs_Ftype11MESSAGEClass,
       WILL_FIRE__inputs_Ftype13IORespClass,
       WILL_FIRE__inputs_Ftype2IOReqClass,
       WILL_FIRE__inputs_Ftype5IOWrClass,
       WILL_FIRE__inputs_Ftype6IOStreamClass,
       WILL_FIRE__inputs_Ftype8IOMaintenanceClass,
       WILL_FIRE__inputs_Ftype9DataStreamingClass,
       WILL_FIRE__inputs_InitReqDataCount,
       WILL_FIRE__inputs_InitReqIfcPkt,
       WILL_FIRE__inputs_MaintenanceRespIfcPkt,
       WILL_FIRE__inputs_TgtRespIfcPkt,
       WILL_FIRE_pkgen_rdy_n;

  // remaining internal signals
  reg [127 : 0] IF_rg_Ftype8MRespInputDelay_97_BITS_102_TO_99__ETC___d743,
		new_value__h11358,
		new_value__h11646,
		new_value__h12248,
		new_value__h12434,
		new_value__h12506,
		new_value__h12692,
		new_value__h15980,
		new_value__h16231,
		new_value__h17225,
		new_value__h17611,
		new_value__h17746,
		new_value__h17917,
		new_value__h18237,
		new_value__h18582,
		new_value__h18929,
		new_value__h19592,
		new_value__h20226;
  reg [8 : 0] IF_IF_wr_InitReqInput_whas_THEN_wr_InitReqInpu_ETC___d194,
	      lv_ByteCountRdWr__h8329,
	      lv_Ftype11ByteCount__h8331,
	      lv_MaintainRespByteCount___1__h10545,
	      lv_MaintainRespByteCount___1__h10590,
	      y__h8476;
  reg [3 : 0] CASE_rg_Ftype8MRespInputDelay_BITS_102_TO_99_0_ETC__q1;
  wire [127 : 0] IF_rg_InitReqInput1Delay_32_BIT_228_16_THEN_IF_ETC___d505,
		 IF_rg_InitReqInput1Delay_32_BIT_228_16_THEN_IF_ETC___d520,
		 _dfoo10,
		 _dfoo20,
		 lv_OutputDataPkt__h20777,
		 new_value__h13519,
		 new_value__h13609,
		 new_value__h13664,
		 new_value__h14132,
		 new_value__h14222,
		 new_value__h14302,
		 new_value__h14948,
		 new_value__h15027,
		 new_value__h15355,
		 new_value__h15447,
		 new_value__h16668,
		 new_value__h16738,
		 x__h20982,
		 x__h20984,
		 x__h20986,
		 x__h20988,
		 x__h20990,
		 x__h20992,
		 x__read_data__h21498,
		 x_wget__h2748,
		 x_wget__h2827,
		 x_wget__h2906,
		 x_wget__h2985,
		 x_wget__h3064,
		 x_wget__h3143,
		 x_wget__h3222,
		 y__h20983,
		 y__h20985,
		 y__h20987,
		 y__h20989,
		 y__h20991,
		 y__h20993;
  wire [63 : 0] IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692,
		data__h14319,
		lv_Ftype13RespData__h15860,
		new_data__h13681;
  wire [8 : 0] IF_IF_wr_MaintainRespInput_whas__5_THEN_wr_Mai_ETC___d342,
	       IF_wr_InitReqInput_whas_AND_wr_InitReqInput_wg_ETC___d182,
	       lv_ByteCountDBell__h8330,
	       lv_ByteCountTT__h10500,
	       lv_RespByteCount__h10026,
	       x__h10041,
	       x__h10512,
	       x__h10522,
	       x__h8362,
	       x__h8389,
	       x__h8636;
  wire [7 : 0] lv_HeaderData__h15338;
  wire [3 : 0] _dfoo12,
	       lv_txrem__h20781,
	       new_value__h15269,
	       new_value__h16188,
	       new_value__h16461,
	       new_value__h16571,
	       x__h21040,
	       x__h21042,
	       x__h21044,
	       x__h21046,
	       x__h21048,
	       x__h21050,
	       x__h21052,
	       x__read_txrem__h21499,
	       x_wget__h5141,
	       x_wget__h5220,
	       x_wget__h5299,
	       x_wget__h5378,
	       x_wget__h5536,
	       x_wget__h5615,
	       x_wget__h5694,
	       y__h21041,
	       y__h21043,
	       y__h21045,
	       y__h21049,
	       y__h21051,
	       y__h21053;
  wire _dfoo11,
       _dfoo25,
       rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273,
       rg_CurrentInitReqByteCount_95_ULE_16___d196,
       rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d664,
       rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d764,
       rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d775,
       wr_EOF_Ftype2Vld_whas__80_AND_wr_EOF_Ftype2Vld_ETC___d898,
       wr_EOF_Ftype2Vld_whas__80_AND_wr_EOF_Ftype2Vld_ETC___d906,
       wr_MaintainRespInput_whas__5_AND_wr_MaintainRe_ETC___d359,
       wr_OutputFtype2Vld_whas__26_AND_wr_OutputFtype_ETC___d844,
       wr_OutputFtype2Vld_whas__26_AND_wr_OutputFtype_ETC___d852,
       wr_SOF_Ftype2Vld_whas__53_AND_wr_SOF_Ftype2Vld_ETC___d863,
       wr_SOF_Ftype2Vld_whas__53_AND_wr_SOF_Ftype2Vld_ETC___d879;

  // action method _inputs_Ftype2IOReqClass
  assign CAN_FIRE__inputs_Ftype2IOReqClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype2IOReqClass = 1'd1 ;

  // action method _inputs_Ftype5IOWrClass
  assign CAN_FIRE__inputs_Ftype5IOWrClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype5IOWrClass = 1'd1 ;

  // action method _inputs_Ftype6IOStreamClass
  assign CAN_FIRE__inputs_Ftype6IOStreamClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype6IOStreamClass = 1'd1 ;

  // action method _inputs_Ftype8IOMaintenanceClass
  assign CAN_FIRE__inputs_Ftype8IOMaintenanceClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype8IOMaintenanceClass = 1'd1 ;

  // action method _inputs_Ftype9DataStreamingClass
  assign CAN_FIRE__inputs_Ftype9DataStreamingClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype9DataStreamingClass = 1'd1 ;

  // action method _inputs_Ftype10MgDOORBELLClass
  assign CAN_FIRE__inputs_Ftype10MgDOORBELLClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype10MgDOORBELLClass = 1'd1 ;

  // action method _inputs_Ftype11MESSAGEClass
  assign CAN_FIRE__inputs_Ftype11MESSAGEClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype11MESSAGEClass = 1'd1 ;

  // action method _inputs_Ftype13IORespClass
  assign CAN_FIRE__inputs_Ftype13IORespClass = 1'd1 ;
  assign WILL_FIRE__inputs_Ftype13IORespClass = 1'd1 ;

  // action method _inputs_InitReqIfcPkt
  assign CAN_FIRE__inputs_InitReqIfcPkt = 1'd1 ;
  assign WILL_FIRE__inputs_InitReqIfcPkt = 1'd1 ;

  // action method _inputs_TgtRespIfcPkt
  assign CAN_FIRE__inputs_TgtRespIfcPkt = 1'd1 ;
  assign WILL_FIRE__inputs_TgtRespIfcPkt = 1'd1 ;

  // action method _inputs_MaintenanceRespIfcPkt
  assign CAN_FIRE__inputs_MaintenanceRespIfcPkt = 1'd1 ;
  assign WILL_FIRE__inputs_MaintenanceRespIfcPkt = 1'd1 ;

  // action method _inputs_InitReqDataCount
  assign CAN_FIRE__inputs_InitReqDataCount = 1'd1 ;
  assign WILL_FIRE__inputs_InitReqDataCount = 1'd1 ;

  // value method pkgen_sof_n_
  assign pkgen_sof_n_ =
	     !CAN_FIRE_RL_rl_FIFOF_First ||
	     !wr_PktGenTranmitIfcFirst$wget[135] ;

  // value method pkgen_eof_n_
  assign pkgen_eof_n_ =
	     !CAN_FIRE_RL_rl_FIFOF_First ||
	     !wr_PktGenTranmitIfcFirst$wget[134] ;

  // value method pkgen_vld_n_
  assign pkgen_vld_n_ =
	     !CAN_FIRE_RL_rl_FIFOF_First ||
	     !wr_PktGenTranmitIfcFirst$wget[133] ;

  // value method pkgen_dsc_n_
  assign pkgen_dsc_n_ = !pkgen_DSC_n ;

  // action method pkgen_rdy_n
  assign CAN_FIRE_pkgen_rdy_n = 1'd1 ;
  assign WILL_FIRE_pkgen_rdy_n = 1'd1 ;

  // value method pkgen_data_
  assign pkgen_data_ =
	     CAN_FIRE_RL_rl_FIFOF_First ?
	       wr_PktGenTranmitIfcFirst$wget[132:5] :
	       128'd0 ;

  // value method pkgen_tx_rem_
  assign pkgen_tx_rem_ =
	     CAN_FIRE_RL_rl_FIFOF_First ?
	       wr_PktGenTranmitIfcFirst$wget[4:1] :
	       4'd0 ;

  // value method pkgen_crf_
  assign pkgen_crf_ = 1'd0 ;

  // value method outputs_RxRdy_From_Dest_
  assign outputs_RxRdy_From_Dest_ = pkgen_rdy_n_value ;

  // submodule ff_TransmitPktIfcFIFO_ff
  SizedFIFO #(.p1width(32'd136),
	      .p2depth(32'd8),
	      .p3cntr_width(32'd3),
	      .guarded(32'd0)) ff_TransmitPktIfcFIFO_ff(.RST(RST_N),
							.CLK(CLK),
							.D_IN(ff_TransmitPktIfcFIFO_ff$D_IN),
							.ENQ(ff_TransmitPktIfcFIFO_ff$ENQ),
							.DEQ(ff_TransmitPktIfcFIFO_ff$DEQ),
							.CLR(ff_TransmitPktIfcFIFO_ff$CLR),
							.D_OUT(ff_TransmitPktIfcFIFO_ff$D_OUT),
							.FULL_N(ff_TransmitPktIfcFIFO_ff$FULL_N),
							.EMPTY_N(ff_TransmitPktIfcFIFO_ff$EMPTY_N));

  // submodule ff_TransmitPktIfcFIFO_firstValid
  RevertReg #(.width(32'd1),
	      .init(1'd1)) ff_TransmitPktIfcFIFO_firstValid(.CLK(CLK),
							    .D_IN(ff_TransmitPktIfcFIFO_firstValid$D_IN),
							    .EN(ff_TransmitPktIfcFIFO_firstValid$EN),
							    .Q_OUT(ff_TransmitPktIfcFIFO_firstValid$Q_OUT));

  // rule RL_rl_Ftype2InputValid
  assign CAN_FIRE_RL_rl_Ftype2InputValid = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype2InputValid = 1'd1 ;

  // rule RL_rl_Ftype5InputValid
  assign CAN_FIRE_RL_rl_Ftype5InputValid = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype5InputValid = 1'd1 ;

  // rule RL_rl_Ftype6InputValid
  assign CAN_FIRE_RL_rl_Ftype6InputValid = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype6InputValid = 1'd1 ;

  // rule RL_rl_Ftype10InputValid
  assign CAN_FIRE_RL_rl_Ftype10InputValid = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype10InputValid = 1'd1 ;

  // rule RL_rl_Ftype13InputValid
  assign CAN_FIRE_RL_rl_Ftype13InputValid = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype13InputValid = 1'd1 ;

  // rule RL_rl_Ftype8InputValid
  assign CAN_FIRE_RL_rl_Ftype8InputValid = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype8InputValid = 1'd1 ;

  // rule RL_rl_Ftype2Generation
  assign CAN_FIRE_RL_rl_Ftype2Generation =
	     rg_Ftype2ReqInputDelay[67:64] == 4'd2 ;
  assign WILL_FIRE_RL_rl_Ftype2Generation = CAN_FIRE_RL_rl_Ftype2Generation ;

  // rule RL_rl_Ftype5Generation
  assign CAN_FIRE_RL_rl_Ftype5Generation =
	     rg_Ftype5WrCsInputDelay[132:129] == 4'd5 ;
  assign WILL_FIRE_RL_rl_Ftype5Generation = CAN_FIRE_RL_rl_Ftype5Generation ;

  // rule RL_rl_Ftype6Generation
  assign CAN_FIRE_RL_rl_Ftype6Generation =
	     rg_Ftype6WrStreamInputDelay[50:47] == 4'd6 ;
  assign WILL_FIRE_RL_rl_Ftype6Generation = CAN_FIRE_RL_rl_Ftype6Generation ;

  // rule RL_rl_Ftype11Generation
  assign CAN_FIRE_RL_rl_Ftype11Generation =
	     rg_Ftype11MsgCsInputDelay[27:24] == 4'd11 ;
  assign WILL_FIRE_RL_rl_Ftype11Generation =
	     CAN_FIRE_RL_rl_Ftype11Generation ;

  // rule RL_rl_InitReqByteCountCalc
  assign CAN_FIRE_RL_rl_InitReqByteCountCalc = 1'd1 ;
  assign WILL_FIRE_RL_rl_InitReqByteCountCalc = 1'd1 ;

  // rule RL_rl_Ftype2InputDelay_1Clk
  assign CAN_FIRE_RL_rl_Ftype2InputDelay_1Clk = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype2InputDelay_1Clk = 1'd1 ;

  // rule RL_rl_Ftype5InputDelay_1Clk
  assign CAN_FIRE_RL_rl_Ftype5InputDelay_1Clk = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype5InputDelay_1Clk = 1'd1 ;

  // rule RL_rl_Ftype6InputDelay_1Clk
  assign CAN_FIRE_RL_rl_Ftype6InputDelay_1Clk = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype6InputDelay_1Clk = 1'd1 ;

  // rule RL_rl_Ftype11InputDelay_1Clk
  assign CAN_FIRE_RL_rl_Ftype11InputDelay_1Clk = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype11InputDelay_1Clk = 1'd1 ;

  // rule RL_rl_Ftype13Generation
  assign CAN_FIRE_RL_rl_Ftype13Generation =
	     rg_Ftype13RespInputDelay[84:81] == 4'd13 ;
  assign WILL_FIRE_RL_rl_Ftype13Generation =
	     CAN_FIRE_RL_rl_Ftype13Generation ;

  // rule RL_rl_Ftype13InputDelay_1Clk
  assign CAN_FIRE_RL_rl_Ftype13InputDelay_1Clk = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype13InputDelay_1Clk = 1'd1 ;

  // rule RL_rl_CurrentRespByteCount
  assign CAN_FIRE_RL_rl_CurrentRespByteCount = 1'd1 ;
  assign WILL_FIRE_RL_rl_CurrentRespByteCount = 1'd1 ;

  // rule RL_rl_Ftype10Generation
  assign CAN_FIRE_RL_rl_Ftype10Generation =
	     rg_Ftype10DoorbellCsInputDelay[27:24] == 4'd10 ;
  assign WILL_FIRE_RL_rl_Ftype10Generation =
	     CAN_FIRE_RL_rl_Ftype10Generation ;

  // rule RL_rl_Ftype10InputDelay_1Clk
  assign CAN_FIRE_RL_rl_Ftype10InputDelay_1Clk = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype10InputDelay_1Clk = 1'd1 ;

  // rule RL_rl_CurrentInitReqByteCountCalc
  assign CAN_FIRE_RL_rl_CurrentInitReqByteCountCalc = 1'd1 ;
  assign WILL_FIRE_RL_rl_CurrentInitReqByteCountCalc = 1'd1 ;

  // rule RL_rl_Ftype8Generation
  assign CAN_FIRE_RL_rl_Ftype8Generation =
	     rg_Ftype8MRespInputDelay[106:103] == 4'd8 ;
  assign WILL_FIRE_RL_rl_Ftype8Generation = CAN_FIRE_RL_rl_Ftype8Generation ;

  // rule RL_rl_Ftype8InputDelay_1Clk
  assign CAN_FIRE_RL_rl_Ftype8InputDelay_1Clk = 1'd1 ;
  assign WILL_FIRE_RL_rl_Ftype8InputDelay_1Clk = 1'd1 ;

  // rule RL_rl_CurrentMaintenanceRespByteCount
  assign CAN_FIRE_RL_rl_CurrentMaintenanceRespByteCount = 1'd1 ;
  assign WILL_FIRE_RL_rl_CurrentMaintenanceRespByteCount = 1'd1 ;

  // rule RL_rl_DataPkt_Valid
  assign CAN_FIRE_RL_rl_DataPkt_Valid = ff_TransmitPktIfcFIFO_ff$FULL_N ;
  assign WILL_FIRE_RL_rl_DataPkt_Valid = ff_TransmitPktIfcFIFO_ff$FULL_N ;

  // rule RL_rl_FIFOF_First
  assign CAN_FIRE_RL_rl_FIFOF_First =
	     ff_TransmitPktIfcFIFO_firstValid$Q_OUT &&
	     (ff_TransmitPktIfcFIFO_ff$EMPTY_N ||
	      ff_TransmitPktIfcFIFO_enqw$whas) ;
  assign WILL_FIRE_RL_rl_FIFOF_First = CAN_FIRE_RL_rl_FIFOF_First ;

  // rule RL_rl_FIFOF_Dequeue
  assign CAN_FIRE_RL_rl_FIFOF_Dequeue =
	     ff_TransmitPktIfcFIFO_firstValid$Q_OUT &&
	     (ff_TransmitPktIfcFIFO_ff$EMPTY_N ||
	      ff_TransmitPktIfcFIFO_enqw$whas) &&
	     (ff_TransmitPktIfcFIFO_ff$EMPTY_N ?
		ff_TransmitPktIfcFIFO_ff$D_OUT[133] :
		ff_TransmitPktIfcFIFO_enqw$wget[133]) &&
	     !pkgen_rdy_n_value ;
  assign WILL_FIRE_RL_rl_FIFOF_Dequeue = CAN_FIRE_RL_rl_FIFOF_Dequeue ;

  // rule RL_disp
  assign CAN_FIRE_RL_disp = 1'd1 ;
  assign WILL_FIRE_RL_disp = 1'd1 ;

  // rule RL_ff_TransmitPktIfcFIFO_enqueue
  assign CAN_FIRE_RL_ff_TransmitPktIfcFIFO_enqueue =
	     ff_TransmitPktIfcFIFO_enqw$whas &&
	     (!CAN_FIRE_RL_rl_FIFOF_Dequeue ||
	      ff_TransmitPktIfcFIFO_ff$EMPTY_N) ;
  assign WILL_FIRE_RL_ff_TransmitPktIfcFIFO_enqueue =
	     CAN_FIRE_RL_ff_TransmitPktIfcFIFO_enqueue ;

  // rule RL_ff_TransmitPktIfcFIFO_dequeue
  assign CAN_FIRE_RL_ff_TransmitPktIfcFIFO_dequeue =
	     CAN_FIRE_RL_rl_FIFOF_Dequeue &&
	     ff_TransmitPktIfcFIFO_ff$EMPTY_N ;
  assign WILL_FIRE_RL_ff_TransmitPktIfcFIFO_dequeue =
	     CAN_FIRE_RL_ff_TransmitPktIfcFIFO_dequeue ;

  // rule RL_rl_CalculateDSC
  assign CAN_FIRE_RL_rl_CalculateDSC =
	     rg_InitReqInput2Delay[225] || rg_TgtRespInput2Delay[130] ;
  assign WILL_FIRE_RL_rl_CalculateDSC = CAN_FIRE_RL_rl_CalculateDSC ;

  // rule RL_rl_InitReqInterfaceInput2Delay
  assign CAN_FIRE_RL_rl_InitReqInterfaceInput2Delay = 1'd1 ;
  assign WILL_FIRE_RL_rl_InitReqInterfaceInput2Delay = 1'd1 ;

  // inlined wires
  assign wr_OutputFtype2DataPkt$whas =
	     WILL_FIRE_RL_rl_Ftype2Generation &&
	     (rg_Ftype2ReqInputDelay[69:68] == 2'b0 ||
	      rg_Ftype2ReqInputDelay[69:68] == 2'b01) &&
	     rg_InitReqInput1Delay[228] ;
  assign wr_OutputFtype5DataPkt$whas =
	     WILL_FIRE_RL_rl_Ftype5Generation &&
	     (rg_Ftype5WrCsInputDelay[134:133] == 2'b0 ||
	      rg_Ftype5WrCsInputDelay[134:133] == 2'b01) ;
  assign wr_OutputFtype6DataPkt$whas =
	     WILL_FIRE_RL_rl_Ftype6Generation &&
	     (rg_Ftype6WrStreamInputDelay[52:51] == 2'b0 ||
	      rg_Ftype6WrStreamInputDelay[52:51] == 2'b01) ;
  assign wr_OutputFtype8MRespDataPkt$whas =
	     WILL_FIRE_RL_rl_Ftype8Generation && _dfoo25 ;
  assign wr_OutputFtype10DataPkt$whas =
	     WILL_FIRE_RL_rl_Ftype10Generation &&
	     (rg_Ftype10DoorbellCsInputDelay[29:28] == 2'b0 ||
	      rg_Ftype10DoorbellCsInputDelay[29:28] == 2'b01) &&
	     rg_InitReqInput1Delay[228] ;
  assign wr_OutputFtype11DataPkt$whas =
	     WILL_FIRE_RL_rl_Ftype11Generation &&
	     (rg_Ftype11MsgCsInputDelay[29:28] == 2'b0 ||
	      rg_Ftype11MsgCsInputDelay[29:28] == 2'b01) ;
  assign wr_OutputFtype13DataPkt$whas =
	     WILL_FIRE_RL_rl_Ftype13Generation &&
	     (rg_Ftype13RespInputDelay[86:85] == 2'b0 &&
	      rg_TgtRespInput1Delay[133] ||
	      rg_Ftype13RespInputDelay[86:85] == 2'b01) ;
  assign wr_OutputFtype8Vld$whas =
	     WILL_FIRE_RL_rl_Ftype8Generation && _dfoo25 ;
  assign wr_OutputFtype11Vld$wget =
	     rg_InitReqInput1Delay[228] ||
	     rg_Ftype11MsgCsInputDelay[23:20] == 4'b0 ||
	     !rg_InitReqInput2Delay[227] ;
  assign wr_SOF_Ftype8Vld$wget =
	     rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d664 ?
	       rg_Ftype8MRespInputDelay[108:107] == 2'b0 ||
	       ((rg_Ftype8MRespInputDelay[102:99] == 4'b0001) ?
		  rg_InitReqInput1Delay[228] :
		  rg_Ftype8MRespInputDelay[102:99] != 4'b0010 ||
		  rg_MaintainRespInput1Delay[132]) :
	       ((rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
		 rg_Ftype8MRespInputDelay[102:99] == 4'b0001) ?
		  rg_InitReqInput1Delay[228] :
		  rg_Ftype8MRespInputDelay[108:107] != 2'b0 ||
		  rg_Ftype8MRespInputDelay[102:99] != 4'b0010 ||
		  rg_MaintainRespInput1Delay[132]) ;
  assign wr_SOF_Ftype8Vld$whas = WILL_FIRE_RL_rl_Ftype8Generation && _dfoo25 ;
  assign wr_SOF_Ftype13Vld$wget =
	     rg_Ftype13RespInputDelay[86:85] == 2'b0 ||
	     rg_TgtRespInput1Delay[133] ;
  assign wr_EOF_Ftype6Vld$whas =
	     WILL_FIRE_RL_rl_Ftype6Generation &&
	     (rg_Ftype6WrStreamInputDelay[52:51] == 2'b0 ||
	      rg_Ftype6WrStreamInputDelay[52:51] == 2'b01) &&
	     !rg_InitReqInput1Delay[228] &&
	     !rg_Ftype6HeaderNotComplete ;
  assign wr_EOF_Ftype8Vld$wget =
	     rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d664 ?
	       rg_Ftype8MRespInputDelay[108:107] == 2'b0 ||
	       ((rg_Ftype8MRespInputDelay[102:99] == 4'b0001) ?
		  !rg_InitReqInput1Delay[228] :
		  rg_Ftype8MRespInputDelay[102:99] != 4'b0010 ||
		  !rg_MaintainRespInput1Delay[132]) :
	       ((rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
		 rg_Ftype8MRespInputDelay[102:99] == 4'b0001) ?
		  !rg_InitReqInput1Delay[228] :
		  rg_Ftype8MRespInputDelay[108:107] != 2'b0 ||
		  rg_Ftype8MRespInputDelay[102:99] != 4'b0010 ||
		  !rg_MaintainRespInput1Delay[132]) ;
  assign wr_EOF_Ftype8Vld$whas = WILL_FIRE_RL_rl_Ftype8Generation && _dfoo25 ;
  assign wr_EOF_Ftype11Vld$wget =
	     !rg_InitReqInput1Delay[228] &&
	     (rg_Ftype11MsgCsInputDelay[23:20] == 4'b0 ||
	      rg_InitReqInput1Delay[227]) ;
  assign wr_EOF_Ftype13Vld$wget =
	     rg_Ftype13RespInputDelay[86:85] == 2'b0 ||
	     !rg_TgtRespInput1Delay[133] ||
	     rg_Ftype13RespInputDelay[80:77] != 4'd8 ;
  assign wr_TxRem_Ftype5$whas =
	     WILL_FIRE_RL_rl_Ftype5Generation &&
	     (rg_Ftype5WrCsInputDelay[134:133] == 2'b0 ||
	      rg_Ftype5WrCsInputDelay[134:133] == 2'b01) &&
	     !rg_InitReqInput1Delay[228] ;
  assign wr_TxRem_Ftype8$whas =
	     WILL_FIRE_RL_rl_Ftype8Generation &&
	     (rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d764 ||
	      _dfoo11) ;
  assign wr_TxRem_Ftype11$whas =
	     WILL_FIRE_RL_rl_Ftype11Generation &&
	     (rg_Ftype11MsgCsInputDelay[29:28] == 2'b0 ||
	      rg_Ftype11MsgCsInputDelay[29:28] == 2'b01 &&
	      !rg_InitReqInput1Delay[228]) ;
  assign wr_PktGenTranmitIfcFirst$wget =
	     ff_TransmitPktIfcFIFO_ff$EMPTY_N ?
	       ff_TransmitPktIfcFIFO_ff$D_OUT :
	       ff_TransmitPktIfcFIFO_enqw$wget ;
  assign ff_TransmitPktIfcFIFO_enqw$wget =
	     { wr_SOF_Ftype2Vld_whas__53_AND_wr_SOF_Ftype2Vld_ETC___d879,
	       wr_EOF_Ftype2Vld_whas__80_AND_wr_EOF_Ftype2Vld_ETC___d906,
	       wr_OutputFtype2Vld_whas__26_AND_wr_OutputFtype_ETC___d852,
	       lv_OutputDataPkt__h20777,
	       lv_txrem__h20781,
	       1'd0 } ;
  assign ff_TransmitPktIfcFIFO_enqw$whas =
	     ff_TransmitPktIfcFIFO_ff$FULL_N &&
	     wr_OutputFtype2Vld_whas__26_AND_wr_OutputFtype_ETC___d852 ;

  // register pkgen_DSC_n
  assign pkgen_DSC_n$D_IN = rg_InitReqInput2Delay[225] ;
  assign pkgen_DSC_n$EN = CAN_FIRE_RL_rl_CalculateDSC ;

  // register pkgen_EOF_n
  assign pkgen_EOF_n$D_IN = 1'b0 ;
  assign pkgen_EOF_n$EN = 1'b0 ;

  // register pkgen_SOF_n
  assign pkgen_SOF_n$D_IN = 1'b0 ;
  assign pkgen_SOF_n$EN = 1'b0 ;

  // register pkgen_VLD_n
  assign pkgen_VLD_n$D_IN = 1'b0 ;
  assign pkgen_VLD_n$EN = 1'b0 ;

  // register rg_CurrentInitReqByteCount
  assign rg_CurrentInitReqByteCount$D_IN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] ?
	       IF_IF_wr_InitReqInput_whas_THEN_wr_InitReqInpu_ETC___d194 :
	       (rg_CurrentInitReqByteCount_95_ULE_16___d196 ?
		  9'd0 :
		  x__h8636) ;
  assign rg_CurrentInitReqByteCount$EN =
	     _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd2 ||
	     _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd5 ||
	     _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd10 ||
	     _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd11 ||
	     !_inputs_InitReqIfcPkt_ireqpkt[228] ;

  // register rg_CurrentMaintainRespByteCount
  assign rg_CurrentMaintainRespByteCount$D_IN =
	     (_inputs_MaintenanceRespIfcPkt_mresppkt[132] ||
	      _inputs_InitReqIfcPkt_ireqpkt[228]) ?
	       x__h10512 :
	       ((rg_CurrentMaintainRespByteCount <= 9'd16) ?
		  9'd0 :
		  x__h10522) ;
  assign rg_CurrentMaintainRespByteCount$EN = 1'd1 ;

  // register rg_CurrentTgtRespByteCount
  assign rg_CurrentTgtRespByteCount$D_IN =
	     _inputs_TgtRespIfcPkt_tgtresppkt[133] ?
	       lv_RespByteCount__h10026 :
	       ((rg_CurrentTgtRespByteCount <= 9'd16) ? 9'd0 : x__h10041) ;
  assign rg_CurrentTgtRespByteCount$EN = 1'd1 ;

  // register rg_DataResponseValid
  assign rg_DataResponseValid$D_IN = 1'b0 ;
  assign rg_DataResponseValid$EN = 1'b0 ;

  // register rg_Ftype10DoorbellCsInputDelay
  assign rg_Ftype10DoorbellCsInputDelay$D_IN =
	     (_inputs_InitReqIfcPkt_ireqpkt[228] &&
	      _inputs_InitReqIfcPkt_ireqpkt[227]) ?
	       _inputs_Ftype10MgDOORBELLClass_pkt :
	       30'd0 ;
  assign rg_Ftype10DoorbellCsInputDelay$EN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] &&
	     _inputs_InitReqIfcPkt_ireqpkt[227] ||
	     rg_CurrentInitReqByteCount <= 9'd8 ;

  // register rg_Ftype10InputValid
  assign rg_Ftype10InputValid$D_IN =
	     rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273 ?
	       rg_Ftype10DoorbellCsInputDelay :
	       30'd0 ;
  assign rg_Ftype10InputValid$EN =
	     rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273 ||
	     rg_CurrentInitReqByteCount == 9'd0 ;

  // register rg_Ftype11DataValid
  assign rg_Ftype11DataValid$D_IN = 64'h0 ;
  assign rg_Ftype11DataValid$EN = 1'b0 ;

  // register rg_Ftype11DataValidDelayed
  assign rg_Ftype11DataValidDelayed$D_IN =
	     (rg_Ftype11MsgCsInputDelay[29:28] == 2'b0) ?
	       rg_Ftype11DataValid :
	       rg_InitReqInputData ;
  assign rg_Ftype11DataValidDelayed$EN = wr_OutputFtype11DataPkt$whas ;

  // register rg_Ftype11InputValid
  assign rg_Ftype11InputValid$D_IN = 30'h0 ;
  assign rg_Ftype11InputValid$EN = 1'b0 ;

  // register rg_Ftype11LastData3Delay
  assign rg_Ftype11LastData3Delay$D_IN = 1'b0 ;
  assign rg_Ftype11LastData3Delay$EN = 1'b0 ;

  // register rg_Ftype11LastDeta2Delay
  assign rg_Ftype11LastDeta2Delay$D_IN = 1'b0 ;
  assign rg_Ftype11LastDeta2Delay$EN = 1'b0 ;

  // register rg_Ftype11MsgCsInputDelay
  assign rg_Ftype11MsgCsInputDelay$D_IN =
	     (_inputs_InitReqIfcPkt_ireqpkt[228] &&
	      _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd11) ?
	       _inputs_Ftype11MESSAGEClass_pkt :
	       30'd0 ;
  assign rg_Ftype11MsgCsInputDelay$EN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] &&
	     _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd11 ||
	     rg_CurrentInitReqByteCount_95_ULE_16___d196 ;

  // register rg_Ftype11_HdrNotComplete
  assign rg_Ftype11_HdrNotComplete$D_IN = 1'd1 ;
  assign rg_Ftype11_HdrNotComplete$EN =
	     WILL_FIRE_RL_rl_Ftype11Generation &&
	     rg_Ftype11MsgCsInputDelay[29:28] == 2'b01 &&
	     rg_InitReqInput1Delay[228] ;

  // register rg_Ftype13InputValid
  always@(rg_CurrentTgtRespByteCount or rg_Ftype13RespInputDelay)
  begin
    case (rg_CurrentTgtRespByteCount)
      9'd8, 9'd16, 9'd24:
	  rg_Ftype13InputValid$D_IN = rg_Ftype13RespInputDelay;
      default: rg_Ftype13InputValid$D_IN =
		   { 23'd0, rg_Ftype13RespInputDelay[63:0] };
    endcase
  end
  assign rg_Ftype13InputValid$EN =
	     rg_CurrentTgtRespByteCount == 9'd24 ||
	     rg_CurrentTgtRespByteCount == 9'd16 ||
	     rg_CurrentTgtRespByteCount == 9'd8 ||
	     rg_CurrentTgtRespByteCount == 9'd0 ;

  // register rg_Ftype13RespData
  assign rg_Ftype13RespData$D_IN = lv_Ftype13RespData__h15860 ;
  assign rg_Ftype13RespData$EN =
	     WILL_FIRE_RL_rl_Ftype13Generation &&
	     (rg_Ftype13RespInputDelay[86:85] == 2'b0 ||
	      rg_Ftype13RespInputDelay[86:85] == 2'b01) ;

  // register rg_Ftype13RespInputDelay
  assign rg_Ftype13RespInputDelay$D_IN =
	     _inputs_TgtRespIfcPkt_tgtresppkt[133] ?
	       _inputs_Ftype13IORespClass_pkt :
	       87'h000000AAAAAAAAAAAAAAAA ;
  assign rg_Ftype13RespInputDelay$EN =
	     _inputs_TgtRespIfcPkt_tgtresppkt[133] ||
	     rg_CurrentTgtRespByteCount == 9'd16 ;

  // register rg_Ftype2InputValid
  assign rg_Ftype2InputValid$D_IN =
	     rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273 ?
	       rg_Ftype2ReqInputDelay :
	       70'd0 ;
  assign rg_Ftype2InputValid$EN =
	     rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273 ||
	     rg_CurrentInitReqByteCount == 9'd0 ;

  // register rg_Ftype2ReqInputDelay
  assign rg_Ftype2ReqInputDelay$D_IN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] ?
	       _inputs_Ftype2IOReqClass_pkt :
	       70'd0 ;
  assign rg_Ftype2ReqInputDelay$EN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] ||
	     rg_CurrentInitReqByteCount_95_ULE_16___d196 ;

  // register rg_Ftype5HeaderNotComplete
  assign rg_Ftype5HeaderNotComplete$D_IN = 1'd1 ;
  assign rg_Ftype5HeaderNotComplete$EN =
	     WILL_FIRE_RL_rl_Ftype5Generation &&
	     rg_Ftype5WrCsInputDelay[134:133] == 2'b01 &&
	     rg_InitReqInput1Delay[228] ;

  // register rg_Ftype5InputValid
  assign rg_Ftype5InputValid$D_IN =
	     rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273 ?
	       rg_Ftype5WrCsInputDelay :
	       { 71'd0, rg_Ftype5WrCsInputDelay[63:0] } ;
  assign rg_Ftype5InputValid$EN =
	     rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273 ||
	     rg_CurrentInitReqByteCount == 9'd0 ;

  // register rg_Ftype5WrCsInputDelay
  assign rg_Ftype5WrCsInputDelay$D_IN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] ?
	       _inputs_Ftype5IOWrClass_pkt :
	       135'h000000000000000000AAAAAAAAAAAAAAAA ;
  assign rg_Ftype5WrCsInputDelay$EN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] ||
	     rg_CurrentInitReqByteCount_95_ULE_16___d196 ;

  // register rg_Ftype6DataInput
  assign rg_Ftype6DataInput$D_IN =
	     _inputs_InitReqIfcPkt_ireqpkt[226] ?
	       _inputs_InitReqIfcPkt_ireqpkt[222:159] :
	       64'd0 ;
  assign rg_Ftype6DataInput$EN = 1'd1 ;

  // register rg_Ftype6DataValid
  assign rg_Ftype6DataValid$D_IN = rg_Ftype6DataInput ;
  assign rg_Ftype6DataValid$EN = 1'd1 ;

  // register rg_Ftype6DataValid1Delayed
  assign rg_Ftype6DataValid1Delayed$D_IN = rg_Ftype6DataValidDelayed ;
  assign rg_Ftype6DataValid1Delayed$EN =
	     WILL_FIRE_RL_rl_Ftype6Generation &&
	     rg_Ftype6WrStreamInputDelay[52:51] == 2'b01 ;

  // register rg_Ftype6DataValidDelayed
  assign rg_Ftype6DataValidDelayed$D_IN = rg_Ftype6DataInput ;
  assign rg_Ftype6DataValidDelayed$EN = wr_OutputFtype6DataPkt$whas ;

  // register rg_Ftype6HeaderNotComplete
  assign rg_Ftype6HeaderNotComplete$D_IN = rg_InitReqInput1Delay[228] ;
  assign rg_Ftype6HeaderNotComplete$EN =
	     WILL_FIRE_RL_rl_Ftype6Generation &&
	     (rg_Ftype6WrStreamInputDelay[52:51] == 2'b0 ||
	      rg_Ftype6WrStreamInputDelay[52:51] == 2'b01) &&
	     (rg_InitReqInput1Delay[228] || rg_Ftype6HeaderNotComplete) ;

  // register rg_Ftype6InputValid
  assign rg_Ftype6InputValid$D_IN = rg_Ftype6WrStreamInputDelay ;
  assign rg_Ftype6InputValid$EN = 1'd1 ;

  // register rg_Ftype6LastData2Delay
  assign rg_Ftype6LastData2Delay$D_IN = rg_InitReqDataCount[4] ;
  assign rg_Ftype6LastData2Delay$EN = 1'd1 ;

  // register rg_Ftype6LastData3Delay
  assign rg_Ftype6LastData3Delay$D_IN = rg_InitReqDataCount[4] ;
  assign rg_Ftype6LastData3Delay$EN = wr_OutputFtype6DataPkt$whas ;

  // register rg_Ftype6LastData4Delay
  assign rg_Ftype6LastData4Delay$D_IN = rg_Ftype6LastData3Delay ;
  assign rg_Ftype6LastData4Delay$EN =
	     WILL_FIRE_RL_rl_Ftype6Generation &&
	     rg_Ftype6WrStreamInputDelay[52:51] == 2'b01 ;

  // register rg_Ftype6WrStreamInputDelay
  assign rg_Ftype6WrStreamInputDelay$D_IN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] ?
	       _inputs_Ftype6IOStreamClass_pkt :
	       53'd0 ;
  assign rg_Ftype6WrStreamInputDelay$EN =
	     _inputs_InitReqIfcPkt_ireqpkt[228] ||
	     rg_InitReqInput1Delay[227] ;

  // register rg_Ftype8MRespInputDelay
  assign rg_Ftype8MRespInputDelay$D_IN =
	     wr_MaintainRespInput_whas__5_AND_wr_MaintainRe_ETC___d359 ?
	       _inputs_Ftype8IOMaintenanceClass_pkt :
	       109'h000000000000AAAAAAAAAAAAAAAA ;
  assign rg_Ftype8MRespInputDelay$EN =
	     wr_MaintainRespInput_whas__5_AND_wr_MaintainRe_ETC___d359 ||
	     rg_CurrentMaintainRespByteCount == 9'd16 ;

  // register rg_Ftype8MRespInputValid
  always@(rg_CurrentMaintainRespByteCount or rg_Ftype8MRespInputDelay)
  begin
    case (rg_CurrentMaintainRespByteCount)
      9'd8, 9'd16, 9'd24:
	  rg_Ftype8MRespInputValid$D_IN = rg_Ftype8MRespInputDelay;
      default: rg_Ftype8MRespInputValid$D_IN =
		   { 45'd0, rg_Ftype8MRespInputDelay[63:0] };
    endcase
  end
  assign rg_Ftype8MRespInputValid$EN =
	     rg_CurrentMaintainRespByteCount == 9'd24 ||
	     rg_CurrentMaintainRespByteCount == 9'd16 ||
	     rg_CurrentMaintainRespByteCount == 9'd8 ||
	     rg_CurrentMaintainRespByteCount == 9'd0 ;

  // register rg_Ftype9_Activate
  assign rg_Ftype9_Activate$D_IN = 1'b0 ;
  assign rg_Ftype9_Activate$EN = 1'b0 ;

  // register rg_InitReqByteCount
  assign rg_InitReqByteCount$D_IN =
	     (_inputs_InitReqIfcPkt_ireqpkt[228] &&
	      _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd2) ?
	       lv_ByteCountRdWr__h8329 :
	       ((_inputs_InitReqIfcPkt_ireqpkt[228] &&
		 _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd5) ?
		  x__h8362 :
		  IF_wr_InitReqInput_whas_AND_wr_InitReqInput_wg_ETC___d182) ;
  assign rg_InitReqByteCount$EN = 1'd1 ;

  // register rg_InitReqDataCount
  assign rg_InitReqDataCount$D_IN = _inputs_InitReqDataCount_value ;
  assign rg_InitReqDataCount$EN = 1'd1 ;

  // register rg_InitReqInput1Delay
  assign rg_InitReqInput1Delay$D_IN = _inputs_InitReqIfcPkt_ireqpkt ;
  assign rg_InitReqInput1Delay$EN = 1'd1 ;

  // register rg_InitReqInput2Delay
  assign rg_InitReqInput2Delay$D_IN = rg_InitReqInput1Delay ;
  assign rg_InitReqInput2Delay$EN = 1'd1 ;

  // register rg_InitReqInputData
  assign rg_InitReqInputData$D_IN = _inputs_InitReqIfcPkt_ireqpkt[222:159] ;
  assign rg_InitReqInputData$EN = 1'd1 ;

  // register rg_MRespHeaderNotComplete
  assign rg_MRespHeaderNotComplete$D_IN =
	     rg_Ftype8MRespInputDelay[108:107] != 2'b0 ;
  assign rg_MRespHeaderNotComplete$EN =
	     WILL_FIRE_RL_rl_Ftype8Generation &&
	     rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d775 ;

  // register rg_MaintainRespInput1Delay
  assign rg_MaintainRespInput1Delay$D_IN =
	     _inputs_MaintenanceRespIfcPkt_mresppkt ;
  assign rg_MaintainRespInput1Delay$EN = 1'd1 ;

  // register rg_MaintainRespInput2Delay
  assign rg_MaintainRespInput2Delay$D_IN = rg_MaintainRespInput1Delay ;
  assign rg_MaintainRespInput2Delay$EN = 1'd1 ;

  // register rg_OutputDataPkt
  assign rg_OutputDataPkt$D_IN = 128'h0 ;
  assign rg_OutputDataPkt$EN = 1'b0 ;

  // register rg_TgtRespInput1Delay
  assign rg_TgtRespInput1Delay$D_IN = _inputs_TgtRespIfcPkt_tgtresppkt ;
  assign rg_TgtRespInput1Delay$EN = 1'd1 ;

  // register rg_TgtRespInput2Delay
  assign rg_TgtRespInput2Delay$D_IN = rg_TgtRespInput1Delay ;
  assign rg_TgtRespInput2Delay$EN = 1'd1 ;

  // register rg_TxRem
  assign rg_TxRem$D_IN = 3'h0 ;
  assign rg_TxRem$EN = 1'b0 ;

  // submodule ff_TransmitPktIfcFIFO_ff
  assign ff_TransmitPktIfcFIFO_ff$D_IN = ff_TransmitPktIfcFIFO_enqw$wget ;
  assign ff_TransmitPktIfcFIFO_ff$ENQ =
	     CAN_FIRE_RL_ff_TransmitPktIfcFIFO_enqueue ;
  assign ff_TransmitPktIfcFIFO_ff$DEQ =
	     CAN_FIRE_RL_ff_TransmitPktIfcFIFO_dequeue ;
  assign ff_TransmitPktIfcFIFO_ff$CLR = CAN_FIRE_RL_rl_CalculateDSC ;

  // submodule ff_TransmitPktIfcFIFO_firstValid
  assign ff_TransmitPktIfcFIFO_firstValid$D_IN = 1'd1 ;
  assign ff_TransmitPktIfcFIFO_firstValid$EN = CAN_FIRE_RL_rl_FIFOF_Dequeue ;

  // remaining internal signals
  assign IF_IF_wr_MaintainRespInput_whas__5_THEN_wr_Mai_ETC___d342 =
	     lv_ByteCountTT__h10500 + 9'd16 ;
  assign IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455 =
	     rg_Ftype5WrCsInputDelay[64] ?
	       rg_Ftype5WrCsInputDelay[63:0] :
	       64'd0 ;
  assign IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692 =
	     rg_Ftype8MRespInputDelay[64] ?
	       rg_Ftype8MRespInputDelay[63:0] :
	       64'd0 ;
  assign IF_rg_InitReqInput1Delay_32_BIT_228_16_THEN_IF_ETC___d505 =
	     rg_InitReqInput1Delay[228] ?
	       new_value__h13519 :
	       (rg_Ftype6HeaderNotComplete ?
		  new_value__h13609 :
		  new_value__h13664) ;
  assign IF_rg_InitReqInput1Delay_32_BIT_228_16_THEN_IF_ETC___d520 =
	     rg_InitReqInput1Delay[228] ?
	       new_value__h14132 :
	       (rg_Ftype6HeaderNotComplete ?
		  new_value__h14222 :
		  new_value__h14302) ;
  assign IF_wr_InitReqInput_whas_AND_wr_InitReqInput_wg_ETC___d182 =
	     (_inputs_InitReqIfcPkt_ireqpkt[228] &&
	      _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd10) ?
	       lv_ByteCountDBell__h8330 :
	       ((_inputs_InitReqIfcPkt_ireqpkt[228] &&
		 _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd11) ?
		  x__h8389 :
		  9'h1FF) ;
  assign _dfoo10 =
	     (rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0010) ?
	       (rg_MaintainRespInput1Delay[132] ?
		  new_value__h18929 :
		  new_value__h18582) :
	       new_value__h19592 ;
  assign _dfoo11 =
	     rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	     rg_Ftype8MRespInputDelay[102:99] == 4'b0001 &&
	     !rg_InitReqInput1Delay[228] ||
	     rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	     (rg_Ftype8MRespInputDelay[102:99] == 4'b0010 &&
	      !rg_MaintainRespInput1Delay[132] ||
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0011 &&
	      rg_MaintainRespInput1Delay[132]) ;
  assign _dfoo12 =
	     (rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0001 &&
	      !rg_InitReqInput1Delay[228]) ?
	       4'b0 :
	       ((rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
		 rg_Ftype8MRespInputDelay[102:99] == 4'b0010 &&
		 !rg_MaintainRespInput1Delay[132]) ?
		  4'b0 :
		  4'b1000) ;
  assign _dfoo20 =
	     (rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0001) ?
	       (rg_InitReqInput1Delay[228] ?
		  new_value__h18237 :
		  new_value__h18582) :
	       _dfoo10 ;
  assign _dfoo25 =
	     rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d664 ||
	     rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	     rg_Ftype8MRespInputDelay[102:99] == 4'b0001 ||
	     rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	     (rg_Ftype8MRespInputDelay[102:99] == 4'b0010 ||
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0011 &&
	      rg_MaintainRespInput1Delay[132]) ;
  assign data__h14319 = { rg_Ftype6DataInput[31:0], 32'h0 } ;
  assign lv_ByteCountDBell__h8330 =
	     (_inputs_InitReqIfcPkt_ireqpkt[224:223] == 2'b10) ?
	       9'd24 :
	       9'd16 ;
  assign lv_ByteCountTT__h10500 =
	     (_inputs_MaintenanceRespIfcPkt_mresppkt[129:128] == 2'b10) ?
	       9'd24 :
	       9'd16 ;
  assign lv_Ftype13RespData__h15860 =
	     rg_Ftype13RespInputDelay[64] ?
	       rg_Ftype13RespInputDelay[63:0] :
	       64'd0 ;
  assign lv_HeaderData__h15338 =
	     rg_Ftype11_HdrNotComplete ?
	       rg_Ftype11DataValidDelayed[7:0] :
	       8'h0 ;
  assign lv_OutputDataPkt__h20777 = x__h20982 | y__h20983 ;
  assign lv_RespByteCount__h10026 =
	     _inputs_TgtRespIfcPkt_tgtresppkt[133] ?
	       ((_inputs_TgtRespIfcPkt_tgtresppkt[12:9] == 4'd8) ?
		  9'd32 :
		  9'd16) :
	       9'h1FF ;
  assign lv_txrem__h20781 = x__h21040 | y__h21041 ;
  assign new_data__h13681 = { rg_Ftype6DataInput[47:0], 16'h0 } ;
  assign new_value__h13519 =
	     (rg_Ftype6WrStreamInputDelay[50:47] == 4'd6) ?
	       { 8'h0,
		 rg_InitReqInput1Delay[157:156],
		 2'b0,
		 rg_Ftype6WrStreamInputDelay[50:47],
		 rg_InitReqInput1Delay[151:144],
		 8'h0,
		 rg_Ftype6WrStreamInputDelay[46:2],
		 1'b0,
		 rg_Ftype6WrStreamInputDelay[1:0],
		 rg_Ftype6DataInput[63:16] } :
	       128'd0 ;
  assign new_value__h13609 =
	     (rg_Ftype6WrStreamInputDelay[50:47] == 4'd6) ?
	       { rg_Ftype6DataValidDelayed[15:0],
		 rg_Ftype6DataInput,
		 48'd0 } :
	       128'd0 ;
  assign new_value__h13664 =
	     (rg_Ftype6WrStreamInputDelay[50:47] == 4'd6) ?
	       { rg_Ftype6DataInput[63:48], new_data__h13681, 48'd0 } :
	       128'd0 ;
  assign new_value__h14132 =
	     (rg_Ftype6WrStreamInputDelay[50:47] == 4'd6) ?
	       { 8'b0,
		 rg_InitReqInput1Delay[157:156],
		 rg_InitReqInput1Delay[224:223],
		 rg_Ftype6WrStreamInputDelay[50:47],
		 rg_InitReqInput1Delay[151:136],
		 16'h0,
		 rg_Ftype6WrStreamInputDelay[46:2],
		 1'b0,
		 rg_Ftype6WrStreamInputDelay[1:0],
		 rg_Ftype6DataInput[63:32] } :
	       128'd0 ;
  assign new_value__h14222 =
	     (rg_Ftype6WrStreamInputDelay[50:47] == 4'd6) ?
	       { rg_Ftype6DataValidDelayed[31:0],
		 rg_Ftype6DataInput,
		 32'h0 } :
	       128'd0 ;
  assign new_value__h14302 =
	     (rg_Ftype6WrStreamInputDelay[50:47] == 4'd6) ?
	       { rg_Ftype6DataInput[63:32], data__h14319, 32'h0 } :
	       128'd0 ;
  assign new_value__h14948 =
	     (rg_Ftype11MsgCsInputDelay[27:24] == 4'd11) ?
	       { 8'h0,
		 rg_InitReqInput1Delay[157:156],
		 rg_InitReqInput1Delay[224:223],
		 rg_Ftype11MsgCsInputDelay[27:24],
		 8'h0,
		 rg_InitReqInput1Delay[151:144],
		 rg_Ftype11MsgCsInputDelay[23:0],
		 rg_InitReqInputData,
		 8'h0 } :
	       128'd0 ;
  assign new_value__h15027 =
	     (rg_Ftype11MsgCsInputDelay[27:24] == 4'd11) ?
	       { rg_InitReqInputData, 64'h0 } :
	       128'd0 ;
  assign new_value__h15269 =
	     (rg_Ftype11MsgCsInputDelay[23:20] == 4'b0) ?
	       rg_Ftype11MsgCsInputDelay[23:20] :
	       (rg_InitReqInput1Delay[227] ? 4'b0111 : 4'b0) ;
  assign new_value__h15355 =
	     (rg_Ftype11MsgCsInputDelay[27:24] == 4'd11) ?
	       { 8'h0,
		 rg_InitReqInput1Delay[157:156],
		 rg_InitReqInput1Delay[224:223],
		 rg_Ftype11MsgCsInputDelay[27:24],
		 16'h0,
		 rg_InitReqInput1Delay[151:136],
		 rg_Ftype11MsgCsInputDelay[23:0],
		 rg_InitReqInputData[63:8] } :
	       128'd0 ;
  assign new_value__h15447 =
	     (rg_Ftype11MsgCsInputDelay[27:24] == 4'd11) ?
	       { lv_HeaderData__h15338, rg_InitReqInputData, 56'h0 } :
	       128'd0 ;
  assign new_value__h16188 =
	     (rg_Ftype13RespInputDelay[80:77] == 4'd8) ? 4'b1100 : 4'b0100 ;
  assign new_value__h16461 =
	     (rg_Ftype13RespInputDelay[80:77] == 4'd8) ? 4'b0 : 4'b0110 ;
  assign new_value__h16571 =
	     (rg_Ftype13RespInputDelay[80:77] == 4'd8) ? 4'b1110 : 4'b0110 ;
  assign new_value__h16668 =
	     (rg_Ftype10DoorbellCsInputDelay[27:24] == 4'd10) ?
	       { 8'h0,
		 rg_InitReqInput1Delay[157:156],
		 rg_InitReqInput1Delay[224:223],
		 rg_Ftype10DoorbellCsInputDelay[27:24],
		 rg_InitReqInput1Delay[151:144],
		 16'd0,
		 rg_Ftype10DoorbellCsInputDelay[23:0],
		 64'h0 } :
	       128'd0 ;
  assign new_value__h16738 =
	     { 8'h0,
	       rg_InitReqInput1Delay[157:156],
	       rg_InitReqInput1Delay[224:223],
	       rg_Ftype10DoorbellCsInputDelay[27:24],
	       rg_InitReqInput1Delay[151:136],
	       24'd0,
	       rg_Ftype10DoorbellCsInputDelay[23:0],
	       48'd0 } ;
  assign rg_CurrentInitReqByteCount_95_EQ_rg_InitReqByt_ETC___d273 =
	     rg_CurrentInitReqByteCount == rg_InitReqByteCount ;
  assign rg_CurrentInitReqByteCount_95_ULE_16___d196 =
	     rg_CurrentInitReqByteCount <= 9'd16 ;
  assign rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d664 =
	     rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	     rg_Ftype8MRespInputDelay[102:99] == 4'b0 &&
	     rg_InitReqInput1Delay[228] ||
	     rg_Ftype8MRespInputDelay[108:107] == 2'b01 &&
	     (rg_Ftype8MRespInputDelay[102:99] == 4'b0 &&
	      rg_InitReqInput1Delay[228] ||
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0001 ||
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0010 ||
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0011 &&
	      rg_MaintainRespInput1Delay[132]) ;
  assign rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d764 =
	     rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	     rg_Ftype8MRespInputDelay[102:99] == 4'b0 &&
	     rg_InitReqInput1Delay[228] ||
	     rg_Ftype8MRespInputDelay[108:107] == 2'b01 &&
	     (rg_Ftype8MRespInputDelay[102:99] == 4'b0 &&
	      rg_InitReqInput1Delay[228] ||
	      rg_Ftype8MRespInputDelay[102:99] != 4'b0 &&
	      (rg_Ftype8MRespInputDelay[102:99] == 4'b0001 &&
	       !rg_InitReqInput1Delay[228] ||
	       rg_Ftype8MRespInputDelay[102:99] == 4'b0010 &&
	       !rg_MaintainRespInput1Delay[132] ||
	       rg_Ftype8MRespInputDelay[102:99] == 4'b0011 &&
	       rg_MaintainRespInput1Delay[132])) ;
  assign rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d775 =
	     rg_Ftype8MRespInputDelay[108:107] == 2'b0 &&
	     rg_Ftype8MRespInputDelay[102:99] == 4'b0010 &&
	     !rg_MaintainRespInput1Delay[132] ||
	     rg_Ftype8MRespInputDelay[108:107] == 2'b01 &&
	     (rg_Ftype8MRespInputDelay[102:99] == 4'b0001 &&
	      rg_InitReqInput1Delay[228] ||
	      rg_Ftype8MRespInputDelay[102:99] == 4'b0010 &&
	      rg_MaintainRespInput1Delay[132]) ;
  assign wr_EOF_Ftype2Vld_whas__80_AND_wr_EOF_Ftype2Vld_ETC___d898 =
	     wr_OutputFtype2DataPkt$whas ||
	     wr_OutputFtype5DataPkt$whas && !rg_InitReqInput1Delay[228] ||
	     wr_EOF_Ftype6Vld$whas && rg_InitReqInput1Delay[227] ||
	     wr_EOF_Ftype8Vld$whas && wr_EOF_Ftype8Vld$wget ||
	     wr_OutputFtype10DataPkt$whas ;
  assign wr_EOF_Ftype2Vld_whas__80_AND_wr_EOF_Ftype2Vld_ETC___d906 =
	     wr_EOF_Ftype2Vld_whas__80_AND_wr_EOF_Ftype2Vld_ETC___d898 ||
	     wr_OutputFtype11DataPkt$whas && wr_EOF_Ftype11Vld$wget ||
	     wr_OutputFtype13DataPkt$whas && wr_EOF_Ftype13Vld$wget ;
  assign wr_MaintainRespInput_whas__5_AND_wr_MaintainRe_ETC___d359 =
	     _inputs_MaintenanceRespIfcPkt_mresppkt[132] ||
	     _inputs_InitReqIfcPkt_ireqpkt[228] &&
	     _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd8 ;
  assign wr_OutputFtype2Vld_whas__26_AND_wr_OutputFtype_ETC___d844 =
	     wr_OutputFtype2DataPkt$whas || wr_OutputFtype5DataPkt$whas ||
	     wr_OutputFtype6DataPkt$whas ||
	     wr_OutputFtype8Vld$whas ||
	     wr_OutputFtype10DataPkt$whas ;
  assign wr_OutputFtype2Vld_whas__26_AND_wr_OutputFtype_ETC___d852 =
	     wr_OutputFtype2Vld_whas__26_AND_wr_OutputFtype_ETC___d844 ||
	     wr_OutputFtype11DataPkt$whas && wr_OutputFtype11Vld$wget ||
	     wr_OutputFtype13DataPkt$whas ;
  assign wr_SOF_Ftype2Vld_whas__53_AND_wr_SOF_Ftype2Vld_ETC___d863 =
	     wr_OutputFtype2DataPkt$whas ||
	     (wr_OutputFtype5DataPkt$whas || wr_OutputFtype6DataPkt$whas) &&
	     rg_InitReqInput1Delay[228] ;
  assign wr_SOF_Ftype2Vld_whas__53_AND_wr_SOF_Ftype2Vld_ETC___d879 =
	     wr_SOF_Ftype2Vld_whas__53_AND_wr_SOF_Ftype2Vld_ETC___d863 ||
	     wr_SOF_Ftype8Vld$whas && wr_SOF_Ftype8Vld$wget ||
	     wr_OutputFtype10DataPkt$whas ||
	     wr_OutputFtype11DataPkt$whas && rg_InitReqInput1Delay[228] ||
	     wr_OutputFtype13DataPkt$whas && wr_SOF_Ftype13Vld$wget ;
  assign x__h10041 = rg_CurrentTgtRespByteCount - 9'd16 ;
  assign x__h10512 =
	     _inputs_MaintenanceRespIfcPkt_mresppkt[132] ?
	       lv_MaintainRespByteCount___1__h10545 :
	       ((_inputs_InitReqIfcPkt_ireqpkt[228] &&
		 _inputs_InitReqIfcPkt_ireqpkt[155:152] == 4'd8) ?
		  lv_MaintainRespByteCount___1__h10590 :
		  9'd0) ;
  assign x__h10522 = rg_CurrentMaintainRespByteCount - 9'd16 ;
  assign x__h20982 = x__h20984 | y__h20985 ;
  assign x__h20984 = x__h20986 | y__h20987 ;
  assign x__h20986 = x__h20988 | y__h20989 ;
  assign x__h20988 = x__h20990 | y__h20991 ;
  assign x__h20990 = x__h20992 | y__h20993 ;
  assign x__h20992 = wr_OutputFtype2DataPkt$whas ? x_wget__h2748 : 128'd0 ;
  assign x__h21040 = x__h21042 | y__h21043 ;
  assign x__h21042 = x__h21044 | y__h21045 ;
  assign x__h21044 = x__h21046 | 4'd0 ;
  assign x__h21046 = x__h21048 | y__h21049 ;
  assign x__h21048 = x__h21050 | y__h21051 ;
  assign x__h21050 = x__h21052 | y__h21053 ;
  assign x__h21052 = wr_OutputFtype2DataPkt$whas ? x_wget__h5141 : 4'd0 ;
  assign x__h8362 = 9'd8 + lv_ByteCountRdWr__h8329 ;
  assign x__h8389 = lv_Ftype11ByteCount__h8331 + y__h8476 ;
  assign x__h8636 = rg_CurrentInitReqByteCount - 9'd16 ;
  assign x__read_data__h21498 =
	     CAN_FIRE_RL_rl_FIFOF_First ?
	       wr_PktGenTranmitIfcFirst$wget[132:5] :
	       128'd0 ;
  assign x__read_txrem__h21499 =
	     CAN_FIRE_RL_rl_FIFOF_First ?
	       wr_PktGenTranmitIfcFirst$wget[4:1] :
	       4'd0 ;
  assign x_wget__h2748 =
	     (rg_Ftype2ReqInputDelay[69:68] == 2'b0) ?
	       new_value__h11358 :
	       new_value__h11646 ;
  assign x_wget__h2827 =
	     (rg_Ftype5WrCsInputDelay[134:133] == 2'b0) ?
	       (rg_InitReqInput1Delay[228] ?
		  new_value__h12248 :
		  new_value__h12434) :
	       (rg_InitReqInput1Delay[228] ?
		  new_value__h12506 :
		  new_value__h12692) ;
  assign x_wget__h2906 =
	     (rg_Ftype6WrStreamInputDelay[52:51] == 2'b0) ?
	       IF_rg_InitReqInput1Delay_32_BIT_228_16_THEN_IF_ETC___d505 :
	       IF_rg_InitReqInput1Delay_32_BIT_228_16_THEN_IF_ETC___d520 ;
  assign x_wget__h2985 =
	     rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d664 ?
	       ((rg_Ftype8MRespInputDelay[108:107] == 2'b0) ?
		  new_value__h17225 :
		  IF_rg_Ftype8MRespInputDelay_97_BITS_102_TO_99__ETC___d743) :
	       _dfoo20 ;
  assign x_wget__h3064 =
	     (rg_Ftype10DoorbellCsInputDelay[29:28] == 2'b0) ?
	       new_value__h16668 :
	       new_value__h16738 ;
  assign x_wget__h3143 =
	     (rg_Ftype11MsgCsInputDelay[29:28] == 2'b0) ?
	       (rg_InitReqInput1Delay[228] ?
		  new_value__h14948 :
		  new_value__h15027) :
	       (rg_InitReqInput1Delay[228] ?
		  new_value__h15355 :
		  new_value__h15447) ;
  assign x_wget__h3222 =
	     (rg_Ftype13RespInputDelay[86:85] == 2'b0) ?
	       new_value__h15980 :
	       (rg_TgtRespInput1Delay[133] ? new_value__h16231 : 128'd0) ;
  assign x_wget__h5141 =
	     (rg_Ftype2ReqInputDelay[69:68] == 2'b0) ? 4'b1010 : 4'b1100 ;
  assign x_wget__h5220 =
	     (rg_Ftype5WrCsInputDelay[134:133] == 2'b0) ? 4'b0010 : 4'b0100 ;
  assign x_wget__h5299 = rg_InitReqInput1Delay[227] ? 4'b1000 : 4'd0 ;
  assign x_wget__h5378 =
	     rg_Ftype8MRespInputDelay_97_BITS_108_TO_107_46_ETC___d764 ?
	       ((rg_Ftype8MRespInputDelay[108:107] == 2'b0) ?
		  4'b1000 :
		  CASE_rg_Ftype8MRespInputDelay_BITS_102_TO_99_0_ETC__q1) :
	       _dfoo12 ;
  assign x_wget__h5536 =
	     (rg_Ftype10DoorbellCsInputDelay[29:28] == 2'b0) ?
	       4'b0110 :
	       4'b1000 ;
  assign x_wget__h5615 =
	     (rg_Ftype11MsgCsInputDelay[29:28] == 2'b0) ?
	       (rg_InitReqInput1Delay[228] ? 4'b0 : new_value__h15269) :
	       new_value__h15269 ;
  assign x_wget__h5694 =
	     (rg_Ftype13RespInputDelay[86:85] == 2'b0) ?
	       new_value__h16188 :
	       (rg_TgtRespInput1Delay[133] ?
		  new_value__h16461 :
		  new_value__h16571) ;
  assign y__h20983 = wr_OutputFtype13DataPkt$whas ? x_wget__h3222 : 128'd0 ;
  assign y__h20985 = wr_OutputFtype11DataPkt$whas ? x_wget__h3143 : 128'd0 ;
  assign y__h20987 = wr_OutputFtype10DataPkt$whas ? x_wget__h3064 : 128'd0 ;
  assign y__h20989 =
	     wr_OutputFtype8MRespDataPkt$whas ? x_wget__h2985 : 128'd0 ;
  assign y__h20991 = wr_OutputFtype6DataPkt$whas ? x_wget__h2906 : 128'd0 ;
  assign y__h20993 = wr_OutputFtype5DataPkt$whas ? x_wget__h2827 : 128'd0 ;
  assign y__h21041 = wr_OutputFtype13DataPkt$whas ? x_wget__h5694 : 4'd0 ;
  assign y__h21043 = wr_TxRem_Ftype11$whas ? x_wget__h5615 : 4'd0 ;
  assign y__h21045 = wr_OutputFtype10DataPkt$whas ? x_wget__h5536 : 4'd0 ;
  assign y__h21049 = wr_TxRem_Ftype8$whas ? x_wget__h5378 : 4'd0 ;
  assign y__h21051 = wr_EOF_Ftype6Vld$whas ? x_wget__h5299 : 4'd0 ;
  assign y__h21053 = wr_TxRem_Ftype5$whas ? x_wget__h5220 : 4'd0 ;
  always@(_inputs_InitReqIfcPkt_ireqpkt)
  begin
    case (_inputs_InitReqIfcPkt_ireqpkt[224:223])
      2'b01: lv_ByteCountRdWr__h8329 = 9'd11;
      2'b10: lv_ByteCountRdWr__h8329 = 9'd15;
      default: lv_ByteCountRdWr__h8329 = 9'd9;
    endcase
  end
  always@(_inputs_InitReqIfcPkt_ireqpkt)
  begin
    case (_inputs_InitReqIfcPkt_ireqpkt[224:223])
      2'b0, 2'b01: lv_Ftype11ByteCount__h8331 = 9'd10;
      default: lv_Ftype11ByteCount__h8331 = 9'd8;
    endcase
  end
  always@(rg_Ftype2ReqInputDelay or rg_InitReqInput1Delay)
  begin
    case (rg_Ftype2ReqInputDelay[63:60])
      4'b0100, 4'b1100, 4'b1101, 4'b1110, 4'b1111:
	  new_value__h11358 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype2ReqInputDelay[67:64],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype2ReqInputDelay[63:0],
		32'h0 };
      default: new_value__h11358 = 128'd0;
    endcase
  end
  always@(rg_Ftype2ReqInputDelay or rg_InitReqInput1Delay)
  begin
    case (rg_Ftype2ReqInputDelay[63:60])
      4'b0100, 4'b1100, 4'b1101, 4'b1110, 4'b1111:
	  new_value__h11646 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype2ReqInputDelay[67:64],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype2ReqInputDelay[63:0],
		16'h0 };
      default: new_value__h11646 = 128'd0;
    endcase
  end
  always@(rg_Ftype13RespInputDelay or
	  rg_TgtRespInput1Delay or lv_Ftype13RespData__h15860)
  begin
    case (rg_Ftype13RespInputDelay[80:77])
      4'd0, 4'd1:
	  new_value__h16231 =
	      { 8'h0,
		rg_TgtRespInput1Delay[62:61],
		rg_TgtRespInput1Delay[129:128],
		rg_Ftype13RespInputDelay[84:81],
		rg_TgtRespInput1Delay[56:41],
		16'h0,
		rg_Ftype13RespInputDelay[80:65],
		64'h0 };
      4'd8:
	  new_value__h16231 =
	      { 8'h0,
		rg_TgtRespInput1Delay[62:61],
		rg_TgtRespInput1Delay[129:128],
		rg_Ftype13RespInputDelay[84:81],
		rg_TgtRespInput1Delay[56:41],
		16'h0,
		rg_Ftype13RespInputDelay[80:65],
		lv_Ftype13RespData__h15860 };
      default: new_value__h16231 = 128'd0;
    endcase
  end
  always@(rg_Ftype13RespInputDelay or
	  rg_TgtRespInput1Delay or lv_Ftype13RespData__h15860)
  begin
    case (rg_Ftype13RespInputDelay[80:77])
      4'd0, 4'd1:
	  new_value__h15980 =
	      { 8'h0,
		rg_TgtRespInput1Delay[62:61],
		rg_TgtRespInput1Delay[129:128],
		rg_Ftype13RespInputDelay[84:81],
		rg_TgtRespInput1Delay[56:49],
		8'b0,
		rg_Ftype13RespInputDelay[80:65],
		80'd0 };
      4'd8:
	  new_value__h15980 =
	      { 8'h0,
		rg_TgtRespInput1Delay[62:61],
		rg_TgtRespInput1Delay[129:128],
		rg_Ftype13RespInputDelay[84:81],
		rg_TgtRespInput1Delay[56:49],
		8'b0,
		rg_Ftype13RespInputDelay[80:65],
		lv_Ftype13RespData__h15860,
		16'h0 };
      default: new_value__h15980 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or rg_InitReqInput1Delay)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0, 4'b0001:
	  new_value__h17225 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		rg_Ftype8MRespInputDelay[86:65],
		50'd0 };
      4'b0010, 4'b0011:
	  new_value__h17225 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		72'd0 };
      default: new_value__h17225 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or rg_MaintainRespInput1Delay)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0, 4'b0001:
	  new_value__h19592 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:45],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		rg_Ftype8MRespInputDelay[86:65],
		50'd0 };
      4'b0010, 4'b0011:
	  new_value__h19592 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:45],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		72'd0 };
      default: new_value__h19592 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or rg_InitReqInput1Delay)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0, 4'b0001:
	  new_value__h17611 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		rg_Ftype8MRespInputDelay[86:65],
		34'd0 };
      4'b0010, 4'b0011:
	  new_value__h17611 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		56'd0 };
      default: new_value__h17611 = 128'd0;
    endcase
  end
  always@(_inputs_InitReqIfcPkt_ireqpkt)
  begin
    case (_inputs_InitReqIfcPkt_ireqpkt[15:12])
      4'd0: y__h8476 = 9'd8;
      4'd1: y__h8476 = 9'd24;
      4'd2: y__h8476 = 9'd40;
      4'd3: y__h8476 = 9'd56;
      4'd4: y__h8476 = 9'd72;
      4'd5: y__h8476 = 9'd88;
      4'd6: y__h8476 = 9'd104;
      4'd7: y__h8476 = 9'd130;
      4'd8: y__h8476 = 9'd146;
      4'd9: y__h8476 = 9'd162;
      4'd10: y__h8476 = 9'd178;
      4'd11: y__h8476 = 9'd194;
      4'd12: y__h8476 = 9'd210;
      default: y__h8476 = 9'd0;
    endcase
  end
  always@(rg_Ftype5WrCsInputDelay or
	  rg_InitReqInput1Delay or
	  IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455)
  begin
    case (rg_Ftype5WrCsInputDelay[128:125])
      4'b0100, 4'b0101:
	  new_value__h12248 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype5WrCsInputDelay[132:129],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype5WrCsInputDelay[128:65],
		IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455[63:32] };
      default: new_value__h12248 = 128'd0;
    endcase
  end
  always@(rg_Ftype5WrCsInputDelay or
	  IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455)
  begin
    case (rg_Ftype5WrCsInputDelay[128:125])
      4'b0100, 4'b0101:
	  new_value__h12434 =
	      { IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455[31:0],
		96'd0 };
      default: new_value__h12434 = 128'd0;
    endcase
  end
  always@(rg_Ftype5WrCsInputDelay or
	  rg_InitReqInput1Delay or
	  IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455)
  begin
    case (rg_Ftype5WrCsInputDelay[128:125])
      4'b0100, 4'b0101:
	  new_value__h12506 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype5WrCsInputDelay[132:129],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype5WrCsInputDelay[128:65],
		IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455[63:48] };
      default: new_value__h12506 = 128'd0;
    endcase
  end
  always@(rg_Ftype5WrCsInputDelay or
	  IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455)
  begin
    case (rg_Ftype5WrCsInputDelay[128:125])
      4'b0100, 4'b0101:
	  new_value__h12692 =
	      { IF_rg_Ftype5WrCsInputDelay_78_BIT_64_80_THEN_r_ETC___d455[47:0],
		80'd0 };
      default: new_value__h12692 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or
	  rg_InitReqInput1Delay or
	  IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0:
	  new_value__h18237 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		rg_Ftype8MRespInputDelay[86:65],
		50'd0 };
      4'b0001:
	  new_value__h18237 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		rg_Ftype8MRespInputDelay[86:65],
		2'b0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:16] };
      4'b0010:
	  new_value__h18237 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		24'h0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:16] };
      4'b0011:
	  new_value__h18237 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:144],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		72'd0 };
      default: new_value__h18237 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or
	  IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0001, 4'b0010:
	  new_value__h18582 =
	      { IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[15:0],
		112'd0 };
      default: new_value__h18582 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or
	  rg_MaintainRespInput1Delay or
	  IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0:
	  new_value__h18929 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:45],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		rg_Ftype8MRespInputDelay[86:65],
		50'd0 };
      4'b0001:
	  new_value__h18929 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:45],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		rg_Ftype8MRespInputDelay[86:65],
		2'b0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:16] };
      4'b0010:
	  new_value__h18929 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:45],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		24'h0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:16] };
      4'b0011:
	  new_value__h18929 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:45],
		8'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		72'd0 };
      default: new_value__h18929 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or
	  rg_InitReqInput1Delay or
	  IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0:
	  new_value__h17746 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		rg_Ftype8MRespInputDelay[86:65],
		34'd0 };
      4'b0001:
	  new_value__h17746 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		rg_Ftype8MRespInputDelay[86:65],
		2'b0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:32] };
      4'b0010:
	  new_value__h17746 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		24'h0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:32] };
      4'b0011:
	  new_value__h17746 =
	      { 8'h0,
		rg_InitReqInput1Delay[157:156],
		rg_InitReqInput1Delay[224:223],
		rg_Ftype8MRespInputDelay[106:103],
		rg_InitReqInput1Delay[151:136],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_InitReqInput1Delay[69:62],
		56'd0 };
      default: new_value__h17746 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or
	  rg_MaintainRespInput1Delay or
	  IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0:
	  new_value__h20226 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:37],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		rg_Ftype8MRespInputDelay[86:65],
		34'd0 };
      4'b0001:
	  new_value__h20226 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:37],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		rg_Ftype8MRespInputDelay[86:65],
		2'b0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:32] };
      4'b0010:
	  new_value__h20226 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:37],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		24'h0,
		IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[63:32] };
      4'b0011:
	  new_value__h20226 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:37],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		56'd0 };
      default: new_value__h20226 = 128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or
	  IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0001, 4'b0010:
	  new_value__h17917 =
	      { IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692[31:0],
		96'h0 };
      default: new_value__h17917 = 128'd0;
    endcase
  end
  always@(_inputs_MaintenanceRespIfcPkt_mresppkt or
	  IF_IF_wr_MaintainRespInput_whas__5_THEN_wr_Mai_ETC___d342 or
	  lv_ByteCountTT__h10500)
  begin
    case (_inputs_MaintenanceRespIfcPkt_mresppkt[56:53])
      4'b0010:
	  lv_MaintainRespByteCount___1__h10545 =
	      IF_IF_wr_MaintainRespInput_whas__5_THEN_wr_Mai_ETC___d342;
      4'b0011: lv_MaintainRespByteCount___1__h10545 = lv_ByteCountTT__h10500;
      default: lv_MaintainRespByteCount___1__h10545 = 9'd0;
    endcase
  end
  always@(_inputs_InitReqIfcPkt_ireqpkt or
	  lv_ByteCountTT__h10500 or
	  IF_IF_wr_MaintainRespInput_whas__5_THEN_wr_Mai_ETC___d342)
  begin
    case (_inputs_InitReqIfcPkt_ireqpkt[53:50])
      4'b0: lv_MaintainRespByteCount___1__h10590 = lv_ByteCountTT__h10500;
      4'b0001:
	  lv_MaintainRespByteCount___1__h10590 =
	      IF_IF_wr_MaintainRespInput_whas__5_THEN_wr_Mai_ETC___d342;
      default: lv_MaintainRespByteCount___1__h10590 = 9'd0;
    endcase
  end
  always@(_inputs_InitReqIfcPkt_ireqpkt or
	  x__h8389 or
	  lv_ByteCountRdWr__h8329 or x__h8362 or lv_ByteCountDBell__h8330)
  begin
    case (_inputs_InitReqIfcPkt_ireqpkt[155:152])
      4'd2:
	  IF_IF_wr_InitReqInput_whas_THEN_wr_InitReqInpu_ETC___d194 =
	      lv_ByteCountRdWr__h8329;
      4'd5:
	  IF_IF_wr_InitReqInput_whas_THEN_wr_InitReqInpu_ETC___d194 =
	      x__h8362;
      4'd10:
	  IF_IF_wr_InitReqInput_whas_THEN_wr_InitReqInpu_ETC___d194 =
	      lv_ByteCountDBell__h8330;
      default: IF_IF_wr_InitReqInput_whas_THEN_wr_InitReqInpu_ETC___d194 =
		   x__h8389;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay or
	  new_value__h17611 or
	  rg_InitReqInput1Delay or
	  new_value__h17746 or
	  new_value__h17917 or
	  rg_MaintainRespInput1Delay or new_value__h20226)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0:
	  IF_rg_Ftype8MRespInputDelay_97_BITS_102_TO_99__ETC___d743 =
	      new_value__h17611;
      4'b0001:
	  IF_rg_Ftype8MRespInputDelay_97_BITS_102_TO_99__ETC___d743 =
	      rg_InitReqInput1Delay[228] ?
		new_value__h17746 :
		new_value__h17917;
      4'b0010:
	  IF_rg_Ftype8MRespInputDelay_97_BITS_102_TO_99__ETC___d743 =
	      rg_MaintainRespInput1Delay[132] ?
		new_value__h20226 :
		new_value__h17917;
      4'b0011:
	  IF_rg_Ftype8MRespInputDelay_97_BITS_102_TO_99__ETC___d743 =
	      { 8'h0,
		rg_MaintainRespInput1Delay[62:61],
		rg_MaintainRespInput1Delay[129:128],
		rg_Ftype8MRespInputDelay[106:103],
		rg_MaintainRespInput1Delay[52:37],
		16'h0,
		rg_Ftype8MRespInputDelay[102:87],
		rg_MaintainRespInput1Delay[20:13],
		56'd0 };
      default: IF_rg_Ftype8MRespInputDelay_97_BITS_102_TO_99__ETC___d743 =
		   128'd0;
    endcase
  end
  always@(rg_Ftype8MRespInputDelay)
  begin
    case (rg_Ftype8MRespInputDelay[102:99])
      4'b0: CASE_rg_Ftype8MRespInputDelay_BITS_102_TO_99_0_ETC__q1 = 4'b1010;
      4'b0001:
	  CASE_rg_Ftype8MRespInputDelay_BITS_102_TO_99_0_ETC__q1 = 4'b0010;
      4'b0010:
	  CASE_rg_Ftype8MRespInputDelay_BITS_102_TO_99_0_ETC__q1 =
	      rg_Ftype8MRespInputDelay[102:99];
      default: CASE_rg_Ftype8MRespInputDelay_BITS_102_TO_99_0_ETC__q1 =
		   4'b1010;
    endcase
  end

  // handling of inlined registers

  always@(posedge CLK)
  begin
    if (RST_N == `BSV_RESET_VALUE)
      begin
        pkgen_DSC_n <= `BSV_ASSIGNMENT_DELAY 1'd0;
	pkgen_EOF_n <= `BSV_ASSIGNMENT_DELAY 1'd0;
	pkgen_SOF_n <= `BSV_ASSIGNMENT_DELAY 1'd0;
	pkgen_VLD_n <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_CurrentInitReqByteCount <= `BSV_ASSIGNMENT_DELAY 9'd0;
	rg_CurrentMaintainRespByteCount <= `BSV_ASSIGNMENT_DELAY 9'd0;
	rg_CurrentTgtRespByteCount <= `BSV_ASSIGNMENT_DELAY 9'd0;
	rg_DataResponseValid <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype10DoorbellCsInputDelay <= `BSV_ASSIGNMENT_DELAY 30'd0;
	rg_Ftype10InputValid <= `BSV_ASSIGNMENT_DELAY 30'd0;
	rg_Ftype11DataValid <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype11DataValidDelayed <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype11InputValid <= `BSV_ASSIGNMENT_DELAY 30'd0;
	rg_Ftype11LastData3Delay <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype11LastDeta2Delay <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype11MsgCsInputDelay <= `BSV_ASSIGNMENT_DELAY 30'd0;
	rg_Ftype11_HdrNotComplete <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype13InputValid <= `BSV_ASSIGNMENT_DELAY
	    87'h000000AAAAAAAAAAAAAAAA;
	rg_Ftype13RespData <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype13RespInputDelay <= `BSV_ASSIGNMENT_DELAY
	    87'h000000AAAAAAAAAAAAAAAA;
	rg_Ftype2InputValid <= `BSV_ASSIGNMENT_DELAY 70'd0;
	rg_Ftype2ReqInputDelay <= `BSV_ASSIGNMENT_DELAY 70'd0;
	rg_Ftype5HeaderNotComplete <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype5InputValid <= `BSV_ASSIGNMENT_DELAY
	    135'h000000000000000000AAAAAAAAAAAAAAAA;
	rg_Ftype5WrCsInputDelay <= `BSV_ASSIGNMENT_DELAY
	    135'h000000000000000000AAAAAAAAAAAAAAAA;
	rg_Ftype6DataInput <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype6DataValid <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype6DataValid1Delayed <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype6DataValidDelayed <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_Ftype6HeaderNotComplete <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype6InputValid <= `BSV_ASSIGNMENT_DELAY 53'd0;
	rg_Ftype6LastData2Delay <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype6LastData3Delay <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype6LastData4Delay <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_Ftype6WrStreamInputDelay <= `BSV_ASSIGNMENT_DELAY 53'd0;
	rg_Ftype8MRespInputDelay <= `BSV_ASSIGNMENT_DELAY
	    109'h000000000000AAAAAAAAAAAAAAAA;
	rg_Ftype8MRespInputValid <= `BSV_ASSIGNMENT_DELAY
	    109'h000000000000AAAAAAAAAAAAAAAA;
	rg_Ftype9_Activate <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_InitReqByteCount <= `BSV_ASSIGNMENT_DELAY 9'h1FF;
	rg_InitReqDataCount <= `BSV_ASSIGNMENT_DELAY 5'd0;
	rg_InitReqInput1Delay <= `BSV_ASSIGNMENT_DELAY 229'd0;
	rg_InitReqInput2Delay <= `BSV_ASSIGNMENT_DELAY 229'd0;
	rg_InitReqInputData <= `BSV_ASSIGNMENT_DELAY 64'd0;
	rg_MRespHeaderNotComplete <= `BSV_ASSIGNMENT_DELAY 1'd0;
	rg_MaintainRespInput1Delay <= `BSV_ASSIGNMENT_DELAY 133'd0;
	rg_MaintainRespInput2Delay <= `BSV_ASSIGNMENT_DELAY 133'd0;
	rg_OutputDataPkt <= `BSV_ASSIGNMENT_DELAY 128'd0;
	rg_TgtRespInput1Delay <= `BSV_ASSIGNMENT_DELAY 134'd256;
	rg_TgtRespInput2Delay <= `BSV_ASSIGNMENT_DELAY 134'd256;
	rg_TxRem <= `BSV_ASSIGNMENT_DELAY 3'd0;
      end
    else
      begin
        if (pkgen_DSC_n$EN)
	  pkgen_DSC_n <= `BSV_ASSIGNMENT_DELAY pkgen_DSC_n$D_IN;
	if (pkgen_EOF_n$EN)
	  pkgen_EOF_n <= `BSV_ASSIGNMENT_DELAY pkgen_EOF_n$D_IN;
	if (pkgen_SOF_n$EN)
	  pkgen_SOF_n <= `BSV_ASSIGNMENT_DELAY pkgen_SOF_n$D_IN;
	if (pkgen_VLD_n$EN)
	  pkgen_VLD_n <= `BSV_ASSIGNMENT_DELAY pkgen_VLD_n$D_IN;
	if (rg_CurrentInitReqByteCount$EN)
	  rg_CurrentInitReqByteCount <= `BSV_ASSIGNMENT_DELAY
	      rg_CurrentInitReqByteCount$D_IN;
	if (rg_CurrentMaintainRespByteCount$EN)
	  rg_CurrentMaintainRespByteCount <= `BSV_ASSIGNMENT_DELAY
	      rg_CurrentMaintainRespByteCount$D_IN;
	if (rg_CurrentTgtRespByteCount$EN)
	  rg_CurrentTgtRespByteCount <= `BSV_ASSIGNMENT_DELAY
	      rg_CurrentTgtRespByteCount$D_IN;
	if (rg_DataResponseValid$EN)
	  rg_DataResponseValid <= `BSV_ASSIGNMENT_DELAY
	      rg_DataResponseValid$D_IN;
	if (rg_Ftype10DoorbellCsInputDelay$EN)
	  rg_Ftype10DoorbellCsInputDelay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype10DoorbellCsInputDelay$D_IN;
	if (rg_Ftype10InputValid$EN)
	  rg_Ftype10InputValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype10InputValid$D_IN;
	if (rg_Ftype11DataValid$EN)
	  rg_Ftype11DataValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11DataValid$D_IN;
	if (rg_Ftype11DataValidDelayed$EN)
	  rg_Ftype11DataValidDelayed <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11DataValidDelayed$D_IN;
	if (rg_Ftype11InputValid$EN)
	  rg_Ftype11InputValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11InputValid$D_IN;
	if (rg_Ftype11LastData3Delay$EN)
	  rg_Ftype11LastData3Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11LastData3Delay$D_IN;
	if (rg_Ftype11LastDeta2Delay$EN)
	  rg_Ftype11LastDeta2Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11LastDeta2Delay$D_IN;
	if (rg_Ftype11MsgCsInputDelay$EN)
	  rg_Ftype11MsgCsInputDelay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11MsgCsInputDelay$D_IN;
	if (rg_Ftype11_HdrNotComplete$EN)
	  rg_Ftype11_HdrNotComplete <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype11_HdrNotComplete$D_IN;
	if (rg_Ftype13InputValid$EN)
	  rg_Ftype13InputValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype13InputValid$D_IN;
	if (rg_Ftype13RespData$EN)
	  rg_Ftype13RespData <= `BSV_ASSIGNMENT_DELAY rg_Ftype13RespData$D_IN;
	if (rg_Ftype13RespInputDelay$EN)
	  rg_Ftype13RespInputDelay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype13RespInputDelay$D_IN;
	if (rg_Ftype2InputValid$EN)
	  rg_Ftype2InputValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype2InputValid$D_IN;
	if (rg_Ftype2ReqInputDelay$EN)
	  rg_Ftype2ReqInputDelay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype2ReqInputDelay$D_IN;
	if (rg_Ftype5HeaderNotComplete$EN)
	  rg_Ftype5HeaderNotComplete <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype5HeaderNotComplete$D_IN;
	if (rg_Ftype5InputValid$EN)
	  rg_Ftype5InputValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype5InputValid$D_IN;
	if (rg_Ftype5WrCsInputDelay$EN)
	  rg_Ftype5WrCsInputDelay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype5WrCsInputDelay$D_IN;
	if (rg_Ftype6DataInput$EN)
	  rg_Ftype6DataInput <= `BSV_ASSIGNMENT_DELAY rg_Ftype6DataInput$D_IN;
	if (rg_Ftype6DataValid$EN)
	  rg_Ftype6DataValid <= `BSV_ASSIGNMENT_DELAY rg_Ftype6DataValid$D_IN;
	if (rg_Ftype6DataValid1Delayed$EN)
	  rg_Ftype6DataValid1Delayed <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6DataValid1Delayed$D_IN;
	if (rg_Ftype6DataValidDelayed$EN)
	  rg_Ftype6DataValidDelayed <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6DataValidDelayed$D_IN;
	if (rg_Ftype6HeaderNotComplete$EN)
	  rg_Ftype6HeaderNotComplete <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6HeaderNotComplete$D_IN;
	if (rg_Ftype6InputValid$EN)
	  rg_Ftype6InputValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6InputValid$D_IN;
	if (rg_Ftype6LastData2Delay$EN)
	  rg_Ftype6LastData2Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6LastData2Delay$D_IN;
	if (rg_Ftype6LastData3Delay$EN)
	  rg_Ftype6LastData3Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6LastData3Delay$D_IN;
	if (rg_Ftype6LastData4Delay$EN)
	  rg_Ftype6LastData4Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6LastData4Delay$D_IN;
	if (rg_Ftype6WrStreamInputDelay$EN)
	  rg_Ftype6WrStreamInputDelay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype6WrStreamInputDelay$D_IN;
	if (rg_Ftype8MRespInputDelay$EN)
	  rg_Ftype8MRespInputDelay <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype8MRespInputDelay$D_IN;
	if (rg_Ftype8MRespInputValid$EN)
	  rg_Ftype8MRespInputValid <= `BSV_ASSIGNMENT_DELAY
	      rg_Ftype8MRespInputValid$D_IN;
	if (rg_Ftype9_Activate$EN)
	  rg_Ftype9_Activate <= `BSV_ASSIGNMENT_DELAY rg_Ftype9_Activate$D_IN;
	if (rg_InitReqByteCount$EN)
	  rg_InitReqByteCount <= `BSV_ASSIGNMENT_DELAY
	      rg_InitReqByteCount$D_IN;
	if (rg_InitReqDataCount$EN)
	  rg_InitReqDataCount <= `BSV_ASSIGNMENT_DELAY
	      rg_InitReqDataCount$D_IN;
	if (rg_InitReqInput1Delay$EN)
	  rg_InitReqInput1Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_InitReqInput1Delay$D_IN;
	if (rg_InitReqInput2Delay$EN)
	  rg_InitReqInput2Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_InitReqInput2Delay$D_IN;
	if (rg_InitReqInputData$EN)
	  rg_InitReqInputData <= `BSV_ASSIGNMENT_DELAY
	      rg_InitReqInputData$D_IN;
	if (rg_MRespHeaderNotComplete$EN)
	  rg_MRespHeaderNotComplete <= `BSV_ASSIGNMENT_DELAY
	      rg_MRespHeaderNotComplete$D_IN;
	if (rg_MaintainRespInput1Delay$EN)
	  rg_MaintainRespInput1Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_MaintainRespInput1Delay$D_IN;
	if (rg_MaintainRespInput2Delay$EN)
	  rg_MaintainRespInput2Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_MaintainRespInput2Delay$D_IN;
	if (rg_OutputDataPkt$EN)
	  rg_OutputDataPkt <= `BSV_ASSIGNMENT_DELAY rg_OutputDataPkt$D_IN;
	if (rg_TgtRespInput1Delay$EN)
	  rg_TgtRespInput1Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_TgtRespInput1Delay$D_IN;
	if (rg_TgtRespInput2Delay$EN)
	  rg_TgtRespInput2Delay <= `BSV_ASSIGNMENT_DELAY
	      rg_TgtRespInput2Delay$D_IN;
	if (rg_TxRem$EN) rg_TxRem <= `BSV_ASSIGNMENT_DELAY rg_TxRem$D_IN;
      end
  end

  // synopsys translate_off
  `ifdef BSV_NO_INITIAL_BLOCKS
  `else // not BSV_NO_INITIAL_BLOCKS
  initial
  begin
    pkgen_DSC_n = 1'h0;
    pkgen_EOF_n = 1'h0;
    pkgen_SOF_n = 1'h0;
    pkgen_VLD_n = 1'h0;
    rg_CurrentInitReqByteCount = 9'h0AA;
    rg_CurrentMaintainRespByteCount = 9'h0AA;
    rg_CurrentTgtRespByteCount = 9'h0AA;
    rg_DataResponseValid = 1'h0;
    rg_Ftype10DoorbellCsInputDelay = 30'h2AAAAAAA;
    rg_Ftype10InputValid = 30'h2AAAAAAA;
    rg_Ftype11DataValid = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype11DataValidDelayed = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype11InputValid = 30'h2AAAAAAA;
    rg_Ftype11LastData3Delay = 1'h0;
    rg_Ftype11LastDeta2Delay = 1'h0;
    rg_Ftype11MsgCsInputDelay = 30'h2AAAAAAA;
    rg_Ftype11_HdrNotComplete = 1'h0;
    rg_Ftype13InputValid = 87'h2AAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype13RespData = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype13RespInputDelay = 87'h2AAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype2InputValid = 70'h2AAAAAAAAAAAAAAAAA;
    rg_Ftype2ReqInputDelay = 70'h2AAAAAAAAAAAAAAAAA;
    rg_Ftype5HeaderNotComplete = 1'h0;
    rg_Ftype5InputValid = 135'h2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype5WrCsInputDelay = 135'h2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype6DataInput = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype6DataValid = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype6DataValid1Delayed = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype6DataValidDelayed = 64'hAAAAAAAAAAAAAAAA;
    rg_Ftype6HeaderNotComplete = 1'h0;
    rg_Ftype6InputValid = 53'h0AAAAAAAAAAAAA;
    rg_Ftype6LastData2Delay = 1'h0;
    rg_Ftype6LastData3Delay = 1'h0;
    rg_Ftype6LastData4Delay = 1'h0;
    rg_Ftype6WrStreamInputDelay = 53'h0AAAAAAAAAAAAA;
    rg_Ftype8MRespInputDelay = 109'h0AAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype8MRespInputValid = 109'h0AAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_Ftype9_Activate = 1'h0;
    rg_InitReqByteCount = 9'h0AA;
    rg_InitReqDataCount = 5'h0A;
    rg_InitReqInput1Delay =
	229'h0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_InitReqInput2Delay =
	229'h0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_InitReqInputData = 64'hAAAAAAAAAAAAAAAA;
    rg_MRespHeaderNotComplete = 1'h0;
    rg_MaintainRespInput1Delay = 133'h0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_MaintainRespInput2Delay = 133'h0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_OutputDataPkt = 128'hAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_TgtRespInput1Delay = 134'h2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_TgtRespInput2Delay = 134'h2AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA;
    rg_TxRem = 3'h2;
  end
  `endif // BSV_NO_INITIAL_BLOCKS
  // synopsys translate_on

  // handling of system tasks

  // synopsys translate_off
  always@(negedge CLK)
  begin
    #0;
    if (RST_N != `BSV_RESET_VALUE)
      if (WILL_FIRE_RL_rl_Ftype8Generation &&
	  rg_Ftype8MRespInputDelay[108:107] == 2'b01 &&
	  rg_Ftype8MRespInputDelay[102:99] == 4'b0010)
	$display("Maintenance Data == %h",
		 IF_rg_Ftype8MRespInputDelay_97_BIT_64_99_THEN__ETC___d692);
    if (RST_N != `BSV_RESET_VALUE)
      $display("\n\tThe IOGen Transmit Packet FIFO Output == %h",
	       { CAN_FIRE_RL_rl_FIFOF_First &&
		 wr_PktGenTranmitIfcFirst$wget[135],
		 CAN_FIRE_RL_rl_FIFOF_First &&
		 wr_PktGenTranmitIfcFirst$wget[134],
		 CAN_FIRE_RL_rl_FIFOF_First &&
		 wr_PktGenTranmitIfcFirst$wget[133],
		 x__read_data__h21498,
		 x__read_txrem__h21499,
		 CAN_FIRE_RL_rl_FIFOF_First &&
		 wr_PktGenTranmitIfcFirst$wget[0] });
  end
  // synopsys translate_on
endmodule  // mkRapidIO_IOPkt_Generation

